(defproject services.local-backend
  "0.1.0-SNAPSHOT"
  :description "Backend for services.local."
  :url "http://gitlab.com/docmenthol/services.local"
  :license {:name "MIT"
            :url  "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/java.jdbc "0.7.9"]
                 [org.xerial/sqlite-jdbc "3.27.2.1"]
                 [ring/ring-core "1.6.3"]
                 [ring-cors "0.1.13"]
                 [ring-basic-authentication "1.0.5"]
                 [ring/ring-json "0.5.0"]
                 [fogus/ring-edn "0.3.0"]
                 [compojure "1.6.0"]
                 [liberator "0.15.1"]
                 [migratus "1.2.3"]
                 [clj-time "0.15.1"]
                 [clojurewerkz/scrypt "1.2.0"]
                 [codox-theme-dark "0.1.1"]]
  :plugins [[lein-ring "0.12.5"]
            [migratus-lein "0.7.2"]
            [lein-codox "0.10.7"]]
  :main ^:skip-aot services.backend.core/main
  :ring {:handler services.backend.core/app
         :port 3000}
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :migratus {:store :database
             :db    {:classname   "org.sqlite.JDBC"
                     :subprotocol "sqlite"
                     :subname     "resources/db.sqlite3"}}
  :codox {:language     :clojure
          :source-paths ["src/services/backend" "src/services/common"]
          :source-uri   "https://gitlab.com/docmenthol/services.local/blob/master/{filepath}#L{line}"
          :metadata     {:doc/format :markdown :doc "FIXME: Write docs!"}
          :themes       [:dark]})
