CREATE TABLE users (
  id           INTEGER PRIMARY KEY,
  username     TEXT UNIQUE NOT NULL,
  passhash     TEXT NOT NULL,
  level        INTEGER DEFAULT 1,
  api_key      TEXT UNIQUE DEFAULT NULL
);

--;;

INSERT INTO users ( username, passhash, level )
VALUES ( "admin",
         "$s0$e0801$KwfgL2KvC38HBtrx7AKcKw==$/MWpw3H43oHu512ZM+Fgl8mO6oIOHUqEHhUK5tLbPps=",
         10 ),
       ( "user_i",
         "$s0$e0801$gGWNljsdU1+4Ss/biO4nJA==$/yPM5xnEZsP01iKk1VKjV59wljg3ZU5P5ZJXXQE+7gs=",
         1 ),
       ( "user_r",
         "$s0$e0801$WFqxyDisx027Wdp61hBxPw==$l9J0Yg4lzUaaLOe5Mzn8P1fFz6ZEOpBsvnniPgsW/Is=",
         1 ),
       ( "user_j",
         "$s0$e0801$TwSSkMKfod3oLqeqPZTMkw==$uR0fnMrL2GZRzCWjvZBRbLKOg/SZ/FB4R1gXa+Bs1ng=",
         1 ),
       ( "user_d",
         "$s0$e0801$34f7nnn63UZRvQ9X99QhcA==$3R5OyP44AhCuKtSCJJTiBCEGVD2XiY1g3Tn/wSheWok=",
         1 );
