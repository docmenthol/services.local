CREATE TABLE session (
  id INTEGER PRIMARY KEY NOT NULL,
  token TEXT NOT NULL,
  user_id INTEGER NOT NULL,
  expires INTEGER NOT NULL
);
