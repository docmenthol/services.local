CREATE TABLE clicker (
  user_id   INTEGER NOT NULL,
  quarks    INTEGER NOT NULL DEFAULT 0,
  neutrons  INTEGER NOT NULL DEFAULT 0,
  protons   INTEGER NOT NULL DEFAULT 0,
  electrons INTEGER NOT NULL DEFAULT 0,
  hydrogen  INTEGER NOT NULL DEFAULT 0,
  helium    INTEGER NOT NULL DEFAULT 0,
  quark_collectors    INTEGER NOT NULL DEFAULT 0,
  neutron_collectors  INTEGER NOT NULL DEFAULT 0,
  proton_collectors   INTEGER NOT NULL DEFAULT 0,
  electron_collectors INTEGER NOT NULL DEFAULT 0,
  hydrogen_collectors INTEGER NOT NULL DEFAULT 0,
  helium_collectors   INTEGER NOT NULL DEFAULT 0
);

-- ;;

INSERT INTO clicker ( user_id )
VALUES ( 1 ),
       ( 2 ),
       ( 3 ),
       ( 4 ),
       ( 5 );
