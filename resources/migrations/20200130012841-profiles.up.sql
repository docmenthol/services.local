CREATE TABLE profiles (
  id        INTEGER PRIMARY KEY,
  user_id   INTEGER NOT NULL,
  player_id INTEGER NOT NULL,
  avatar    TEXT DEFAULT "_noavatar.png"
);

--;;

INSERT INTO profiles ( user_id, player_id )
VALUES ( 1, 1 ),
       ( 2, 2 ),
       ( 3, 3 ),
       ( 4, 4 );
