CREATE TABLE games (
  id       INTEGER PRIMARY KEY,
  name     TEXT NOT NULL,
  units    TEXT NOT NULL,
  prefix   INTEGER NOT NULL DEFAULT 0,
  adjacent INTEGER NOT NULL DEFAULT 0
);

--;;

INSERT INTO games ( id, name, units, prefix, adjacent )
VALUES ( 1, "H3VR Boomskee - Full Game", "points", 0, 0 ),
       ( 2, "H3VR Boomskee - One Grenade", "points", 0, 0 ),
       ( 3, "Xortex 26XX - Infinite", "points", 0, 0 ),
       ( 4, "Slingshot", "$", 1, 1 ),
       ( 5, "Longbow - Points", "points", 0, 0 ),
       ( 6, "Longbow - Wave", "Wave", 1, 0 ),
       ( 7, "Gorn - Endless", "kills", 0, 0 ),
       ( 8, "Gorn - rock%", "kills", 0, 0 ),
       ( 9, "Ping Pong - Quadrants", "points", 0, 0 );
