CREATE TABLE locks (
  id    INTEGER PRIMARY KEY,
  name  INTEGER NOT NULL,
  level INTEGER NOT NULL
);

--;;

INSERT INTO locks ( name, level )
VALUES ( "Acrylic Training Lock", 0 ),
       ( "Master No. 3", 1 ),
       ( "Brinks R70", 1 ),
       ( "Master 140", 2 ),
       ( "Abus 55/30", 2 ),
       ( "Abus 83/40", 3 ),
       ( "Abus 41/40", 3 ),
       ( "Master 410", 4 ),
       ( "Abus 72/40", 4 ),
       ( "American Lock 1106", 5 ),
       ( "Abus Plus", 7 );

--;;

CREATE TABLE picks (
  lock_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  UNIQUE(lock_id, user_id)
);

--;;

INSERT INTO picks ( lock_id, user_id )
VALUES ( 1, 1 ),
       ( 2, 1 ),
       ( 3, 1 ),
       ( 5, 1 ),
       ( 1, 3 ),
       ( 2, 3 ),
       ( 3, 3 ),
       ( 5, 3 ),
       ( 1, 5 ),
       ( 2, 5 ),
       ( 3, 5 ),
       ( 5, 5 );
