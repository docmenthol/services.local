CREATE TABLE conditions (
  dt          INTEGER PRIMARY KEY,
  temperature REAL NOT NULL,
  lumens      INTEGER NOT NULL
);

--;;

INSERT INTO conditions ( dt, temperature, lumens )
VALUES ( 1560026852205, 22.1, 10 ),
       ( 1560026870205, 22.5, 10 ),
       ( 1560026888205, 23.2, 11 ),
       ( 1560026906205, 24.8, 12 ),
       ( 1560026924205, 23.7, 12 ),
       ( 1560026959375, 22.5, 11 );
