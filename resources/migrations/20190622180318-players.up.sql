CREATE TABLE players (
  id      INTEGER PRIMARY KEY,
  user_id INTEGER DEFAULT NULL,
  pd      REAL DEFAULT NULL, -- pupilary distance
  name    TEXT UNIQUE NOT NULL
);

--;;

INSERT INTO players ( id, user_id, pd, name )
VALUES ( 1, 1,    62.2, "Admin_C"  ),
       ( 2, 2,    64.3, "Player_I" ),
       ( 3, 3,    NULL, "Player_R" ),
       ( 4, 4,    NULL, "Player_J" ),
       ( 5, NULL, NULL, "Team_RC"  ),
       ( 6, NULL, NULL, "Team_CR"  );
