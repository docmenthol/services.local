(ns
  ^{:doc "Leaderboard backend service."}
  services.backend.lockpick
  (:require [liberator.core :refer [defresource]]
            [compojure.core :refer [defroutes GET POST context]]
            [services.backend.db :as db]
            [services.backend.users :refer [user-authorized? get-user-by-id]]))

(defn get-all-locks
  "Returns all locks."
  []
  (db/query "SELECT * FROM locks"))

(defn get-all-picks
  "Returns all picks."
  []
  (db/query
    "SELECT users.username AS pick_name, locks.name as lock_name, picks.*
     FROM picks
     INNER JOIN users ON users.id = picks.user_id
     INNER JOIN locks ON locks.id = picks.lock_id"))

(defn get-single-pick
  [id]
  (first
    (db/query
      ["SELECT users.username AS pick_name, locks.name as lock_name, picks.*
             FROM picks
             INNER JOIN users ON users.id = picks.user_id
             INNER JOIN locks ON locks.id = picks.lock_id
             WHERE picks.user_id = ?" id])))

(defn get-single-lock
  [id]
  (first (db/query ["SELECT * FROM locks WHERE id = ?" id])))

(defn get-locks-for-pick
  [id]
  (db/query
    ["SELECT users.username AS pick_name, locks.name as lock_name, picks.*
      FROM picks
      INNER JOIN users ON users.id = picks.user_id
      INNER JOIN locks ON locks.id = picks.lock_id
      WHERE picks.user_id = ?" id]))

(defn get-picks-for-lock
  [id]
  (db/query
    ["SELECT users.username AS pick_name, locks.name as lock_name, picks.*
      FROM picks
      INNER JOIN users ON users.id = picks.user_id
      INNER JOIN locks ON locks.id = picks.lock_id
      WHERE picks.lock_id = ?" id]))

(defn to-pick
  [user-id lock-id]
  {:user_id user-id :lock_id lock-id})

(defn update-locks-for-pick
  "Updates a user's successful picks."
  [user-id locks]
  (let [picks (map (partial to-pick user-id) locks)]
    (db/delete! :picks ["user_id = ?" user-id])
    (db/insert-multi!
      :picks
      (vec picks))))


(defresource
  ^{:doc "All locks REST resource."}
  all-locks
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? user-authorized?
  :handle-ok (fn [_] (get-all-locks)))

(defresource
  ^{:doc "All picks REST resource."}
  all-picks
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? user-authorized?
  :handle-ok (fn [_] (get-all-picks)))

(defresource
  ^{:doc "Scores for single player REST resource."}
  locks-for-pick
  [{{:keys [id]} :params}]
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? user-authorized?
  :exists? (fn [_]
             {::locks (get-locks-for-pick id)})
  :handle-ok ::locks)

(defresource
  ^{:doc "Scores for single player REST resource."}
  picks-for-lock
  [{{:keys [id]} :params}]
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? user-authorized?
  :exists? (fn [_]
             (if (get-single-lock id)
               {::picks (get-picks-for-lock id)}))
  :handle-ok ::picks)

(defresource
  ^{:doc "Updates the picks for a user."}
  update-picks
  [{{:keys [user_id locks]} :params}]
  :allowed-methods [:post]
  :available-media-types ["application/json" "application/edn"]
  :authorized user-authorized?
  :exists? (fn [_] (get-user-by-id user_id))
  :post!
  (fn [_]
    (update-locks-for-pick user_id locks))
  :handle-ok (fn [_] (get-locks-for-pick user_id)))

(defroutes
  ^{:doc "Lockpick route definitions."}
  lockpick-routes
  (context "/lockpick" []
    (GET "/locks" [] all-locks)
    (GET "/picks" [] all-picks)
    (GET "/locks/pick/:id{[0-9+]}" [id] locks-for-pick)
    (GET "/picks/lock/:id{[0-9+]}" [id] picks-for-lock)
    (context "/update" []
      (POST "/picks" [user_id locks] update-picks))))
      ; (POST "/lock"  [lock]             update-lock))))
