(ns
  ^{:doc "Leaderboard backend service."}
  services.backend.leaderboards
  (:require [liberator.core :refer [defresource]]
            [compojure.core :refer [defroutes GET POST context]]
            [services.backend.db :as db]
            [services.backend.users :refer [user-authorized?]]))

(defn get-all-players
  "Returns all existing players."
  []
  (db/query "SELECT players.*, users.username
             FROM players
             LEFT JOIN users ON players.user_id = users.id"))

(defn get-single-player
  "Finds a single player by their ID."
  [id]
  (first
    (db/query ["SELECT players.*, users.username
                FROM players
                LEFT JOIN users ON players.user_id = users.id
                WHERE players.id = ?
                LIMIT 1" id])))

(defn get-all-games
  "Returns all existing games."
  []
  (db/query "SELECT * FROM games"))

(defn get-single-game
  "Finds a single game by its ID."
  [id]
  (first
    (db/query ["SELECT *
                FROM games
                WHERE id = ?
                LIMIT 1" id])))

(defn get-all-scores
  "Returns all existing scores."
  []
  (db/query
    "SELECT scores.*, games.name as game_name, games.units,
            games.prefix, players.name as player_name
    FROM scores
    INNER JOIN games ON scores.game_id = games.id
    INNER JOIN players ON scores.player_id = players.id"))

(defn get-scores-for-player
  "Returns all scores for a given player by player ID."
  [id]
  (db/query ["SELECT s.id, s.score, s.game_id, s.player_id,
                     p.name AS player_name, g.name AS game_name,
                     g.prefix, g.units, g.adjacent
              FROM scores AS s
              INNER JOIN players AS p ON p.id = s.player_id
              INNER JOIN games AS g ON g.id = s.game_id
              WHERE s.player_id = ?
              ORDER BY s.score DESC" id]))

(defn get-scores-for-game
  "Returns all scores for a given game by game ID."
  [id]
  (db/query ["SELECT s.id, s.score, s.game_id, s.player_id,
                     p.name AS player_name, g.name AS game_name,
                     g.prefix, g.units, g.adjacent
              FROM scores AS s
              INNER JOIN players AS p ON p.id = s.player_id
              INNER JOIN games AS g ON g.id = s.game_id
              WHERE s.game_id = ?
              ORDER BY s.score DESC" id]))

(defn update-player-entity
  "Updates a full player record using the supplied player ID."
  [player]
  (db/update! :players player ["id = ?" (:id player)]))

(defn create-game-entity
  "Created a new game record."
  [game]
  (db/insert! :games game))

(defn create-player-entity
  "Created a new player record."
  [player]
  (db/insert! :players player))

(defn create-score-entity
  "Created a new score record."
  [score]
  (db/insert! :scores score))

(defn booleanize-score
  [{:keys [prefix adjacent] :as score}]
  (assoc
    score
    :prefix (= 1 prefix)
    :adjacent (= 1 adjacent)))

(defn update-game-entity
  "Updates an existing game record."
  [game]
  (db/update! :games game ["id = ?" (:id game)]))

(defresource
  ^{:doc "All players REST resource."}
  all-players
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (fn [_] true)
  :handle-ok (fn [_] (get-all-players)))

(defresource
  ^{:doc "Single player REST resource."}
  single-player
  [{{:keys [id]} :params}]
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (fn [_] true)
  :exists? (fn [_]
             (if-let [player (get-single-player id)]
               {::player player}))
  :handle-ok ::player)

(defresource
  ^{:doc "All games REST resource."}
  all-games
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (fn [_] true)
  :handle-ok (fn [_] (get-all-games)))

(defresource
  ^{:doc "Single game REST resource."}
  single-game
  [{{:keys [id]} :params}]
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (fn [_] true)
  :exists? (fn [_]
             (if-let [game (get-single-game id)]
               {::game game}))
  :handle-ok ::game)

(defresource
  ^{:doc "All Scores REST resource."}
  all-scores
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (fn [_] true)
  :handle-ok (fn [_] (get-all-scores)))

(defresource
  ^{:doc "Scores for single player REST resource."}
  scores-for-player
  [{{:keys [id]} :params}]
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (fn [_] true)
  :exists? (fn [_]
             (if (get-single-player id)
               {::scores
                (map
                  booleanize-score
                  (get-scores-for-player id))}))
  :handle-ok ::scores)

(defresource
  ^{:doc "Scores for single game REST resource."}
  scores-for-game
  [{{:keys [id]} :params}]
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (fn [_] true)
  :exists? (fn [_]
             (if (get-single-game id)
               {::scores
                (map
                  booleanize-score
                  (get-scores-for-game id))}))
  :handle-ok ::scores)

; (defresource
;   ^{:doc "Update player REST resource."}
;   update-player
;   [{{:keys [id user-id name]} :params}]
;   :allowed-methods [:post]
;   :available-media-types ["application/json" "application/edn"]
;   :authorized? (fn [_] true)
;   :exists? (fn [_]
;              (if-let [player (get-single-player id)]
;                {::new-player
;                 (cond-> player
;                   (some? user-id) (assoc :user-id user-id)
;                   (some? name)    (assoc :name name))}))
;   :post! (fn [ctx] (update-player-entity (::new-player ctx))))

(defresource
  ^{:doc "Update game REST resource."}
  update-game
  [{{:keys [id name units prefix]} :params}]
  :allowed-methods [:post]
  :available-media-types ["application/json" "application/edn"]
  :authorized? user-authorized?
  :exists? (fn [_]
             (if-let [game (get-single-game id)]
               {::new-game
                (cond-> game
                  (some? name)  (assoc :name name)
                  (some? units) (assoc :units units)
                  (some? prefix) (assoc :prefix prefix))}))
  :post! (fn [ctx] (update-game-entity (::new-game ctx)))
  :handle-ok ::new-game)

; (defresource
;   ^{:doc "Create game REST resource."}
;   create-game
;   [{{:keys [name units]} :params}]
;   :allowed-methods [:post]
;   :available-media-types ["application/json" "application/edn"]
;   :authorized? user-authorized?
;   :exists? (fn [_]
;              {::new-game {:name name :units units}})
;   :post! (fn [ctx] (create-game-entity (::new-game ctx))))
;
; (defresource
;   ^{:doc "Create player REST resource."}
;   create-player
;   [{{:keys [user-id name]} :params}]
;   :allowed-methods [:post]
;   :available-media-types ["application/json" "application/edn"]
;   :authorized? user-authorized?
;   :exists? (fn [_]
;              {::new-player {:name name :user-id user-id}})
;   :post! (fn [ctx] (create-player-entity (::new-player ctx))))
;
; (defresource
;   ^{:doc "Create score REST resource."}
;   create-score
;   [{{:keys [game-id player-id score]} :params}]
;   :allowed-methods [:post]
;   :available-media-types ["application/json" "application/edn"]
;   :authorized? user-authorized?
;   :exists? (fn [_]
;              {::new-game {:game-id game-id :player-id player-id :score score}})
;   :post! (fn [ctx] (create-score-entity (::new-score ctx))))

(defroutes
  ^{:doc "Leaderboards route definitions."}
  leaderboards-routes
  (context "/leaderboards" []
    (context "/players" []
      (GET "/"            []   all-players)
      (GET "/:id{[0-9]+}" [id] single-player))
      ; (POST "/create"             [user-id name]    create-player)
      ; (POST "/update/:id{[0-9+]}" [id user-id name] update-player))
    (context "/games" []
      (GET "/"            []   all-games)
      (GET "/:id{[0-9]+}" [id] single-game)
      ; (POST "/create"             [name units]           create-game)
      (POST "/update/:id{[0-9+]}" [id name units prefix] update-game))
    (context "/scores" []
      (GET "/"                   [] all-scores)
      (GET "/player/:id{[0-9+]}" [id] scores-for-player)
      (GET "/game/:id{[0-9+]}"   [id] scores-for-game))))
      ; (POST "/create"             [game-id player-id score]     create-score)
      ; (POST "/update"             [id player-id game-id scores] update-score))))
