(ns
  ^{:doc "User auth and registration backend service."}
  services.backend.users
  (:require [liberator.core :refer [defresource]]
            [compojure.core :refer [defroutes GET POST context]]
            [services.backend.crypt :as crypt]
            [services.backend.db :as db]
            [services.common.time :refer [current-timestamp]]))

(defn user-exists?
  "Determines if a username is currently in use."
  [username]
  (seq
    (db/query
      ["SELECT id FROM users WHERE username = ? LIMIT 1" username])))

(defn get-all-users
  "Returns all registered users."
  []
  (db/query "SELECT id, username, level, api_key FROM users"))

(defn get-user-by-username
  "Finds a single user by username."
  [username]
  (first
    (db/query
      ["SELECT id, username, level
        FROM users
        WHERE username = ?
        LIMIT 1"
       username])))

(defn get-user-by-id
  "Finds a single user by user ID."
  [id]
  (first
    (db/query
      ["SELECT id, username, level
        FROM users
        WHERE id = ?
        LIMIT 1"
       id])))

(defn create-user
  "Creates a user in the database, hashing their password in the process."
  [username password]
  (let [hash (crypt/encrypt password)]
    (db/insert! :users {:username username :passhash hash})))

(defn set-display-name
  "Updates a user's display name. Used mostly for the leaderboards service."
  [id name]
  (db/update! :user {:display_name name} ["id = ?" id]))

(defn password-valid?
  "Verifies a provided password."
  [username password]
  (some->>
    (db/query ["SELECT passhash
                FROM users
                WHERE username = ?
                LIMIT 1"
               username])
    first
    :passhash
    (crypt/verify password)))

(defn get-session
  "Finds a session by the attached user ID."
  [user-id]
  (db/delete! :session ["expires <= ?" (current-timestamp)])
  (first
    (db/query
     ["SELECT s.token, s.user_id, u.username, u.level, s.expires
       FROM session AS s
       INNER JOIN users AS u ON u.id = s.user_id
       WHERE s.user_id = ?
       LIMIT 1"
      user-id])))

(defn clear-expired-sessions
  "Clears all expired sessions."
  []
  (db/delete! :session ["expires <= ?" (current-timestamp)]))

(defn session-exists?
  "Determines if an existing session is still valid."
  [token]
  (clear-expired-sessions)
  (seq
    (db/query
      ["SELECT * FROM session WHERE token = ?"
       token])))

(defn create-session
  "Creates a session for a user by user ID."
  [user-id]
  (let [time (current-timestamp)
        expires (+ time (* 24 60 60 1000))
        token (crypt/encrypt (str "services.local-" (/ expires time)))] ; just for fun
    (db/delete! :session ["user_id = ?" user-id])
    (db/insert! :session {:token token :user_id user-id :expires expires})))

(defn create-and-get-session
  "Creates and returns a session for a user by user ID."
  [user-id]
  (create-session user-id)
  (get-session user-id))

(defn delete-session
  "Clears a session for a user ID."
  [user-id]
  (db/delete! :session ["user_id = ?" user-id]))

(defn get-user-by-session
  "Finds a user given a session."
  [session]
  (get-user-by-id (:user-id session)))

(defn get-token-from-ctx
  [ctx]
  (get-in ctx [:request :headers "x-token"]))

(defn access-level
  "Creates a handler for :authorized? for a given access level."
  [level]
  (fn
    [ctx]
    (if-let [token (get-token-from-ctx ctx)]
      (seq
        (db/query
          ["SELECT s.id
            FROM session AS s
            INNER JOIN users AS u ON u.id = s.user_id
            WHERE s.expires < ? AND s.token = ? AND u.level = ?"
           (current-timestamp)
           token
           level]))
      false)))

(defn user-authorized?
  [ctx]
  (if-let [token (get-token-from-ctx ctx)]
    (session-exists? token)
    false))

(defn user-authorized-and-is?
  [ctx user-id]
  (if-let [token (get-token-from-ctx ctx)]
    (if (session-exists? token)
      (get-user-by-id user-id)
      false)
    false))

(defresource
  ^{:doc "All users REST resource."}
  all-users
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (access-level 2)
  :handle-ok (fn [_] (get-all-users)))

(defresource
  ^{:doc "Single user by ID REST resource."}
  user-by-id
  [{{:keys [id]} :params}]
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (access-level 2)
  :exists? (fn [ctx]
             (if-let [user (get-user-by-id id)]
               {::user user}))
  :handle-ok ::user
  :handle-not-found {:error "user not found"})

(defresource
  ^{:doc "Single user by username REST resource."}
  user-by-name
  [{{:keys [name]} :params}]
  :allowed-methods [:get]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (access-level 2)
  :exists? (fn [_]
             (if-let [user (get-user-by-username name)]
               {::user user}))
  :handle-ok ::user
  :handle-not-found {:error (str "user " name " not found")})

(defresource
  ^{:doc "Login REST resource."}
  login
  [{{:keys [username password]} :params}]
  :allowed-methods [:post]
  :available-media-types ["application/json" "application/edn"]
  :exists? (fn [_] (password-valid? username password))
  :post!
  (fn [_]
    {::session (-> (get-user-by-username username)
                   :id
                   (create-and-get-session))})
  :new? false
  :respond-with-entity? true
  :handle-ok ::session
  :handle-not-found {:error "login failed"})

(defresource
  ^{:doc "Logout REST resource."}
  logout
  [{{:keys [user-id session-id]} :params}]
  :allowed-methods [:post]
  :exists? (fn [_] (session-exists? session-id))
  :available-media-types ["application/json" "application/edn"]
  :new? false
  :respond-with-entity? true
  :handle-ok (fn [_]
               (delete-session user-id)
               {:goodbye "thanks for coming."}))

(defresource
  ^{:doc "User registration REST resource."}
  register
  [{{:keys [username password spam]} :params}]
  :allowed-methods [:post]
  :available-media-types ["application/json" "application/edn"]
  :authorized? (fn [_] (empty? spam))
  :post! (fn [_]
           (create-user username password)
           {::user (get-user-by-username username)})
  :handle-created ::user
  :handle-unauthorized {:error "there was an error processing this request"})

(defroutes
  ^{:doc "User management route definitions."}
  users-routes
  (POST "/login"    [] login)
  (POST "/logout"   [] logout)
  (POST "/register" [] register)
  (context "/users" []
    (GET  "/"               []      all-users)
    (GET  "/id/:id{[0-9]+}" [id]    user-by-id)
    (GET  "/name/:name"     [name]  user-by-name)))
    ; (POST "/update" [id username level api-key] update-user)))
