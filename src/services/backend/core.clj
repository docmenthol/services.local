(ns
  ^{:doc "Main backend service."}
  services.backend.core
  (:require [liberator.core :refer [defresource]]
            [liberator.dev :refer [wrap-trace]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.edn :refer [wrap-edn-params]]
            [ring.middleware.json :refer [wrap-json-params]]
            [compojure.core :refer [defroutes routes context GET]]
            [compojure.route :refer [not-found]]
            [services.backend.users :refer [users-routes]]
            [services.backend.conditions :refer [conditions-routes]]
            [services.backend.leaderboards :refer [leaderboards-routes]]
            [services.backend.lockpick :refer [lockpick-routes]]
            [services.backend.clicker :refer [clicker-routes]]
            [services.backend.db :as db]
            [services.backend.crypt :as crypt]))

(defresource
  ^{:doc "Placeholder root REST resource."}
  index
  :allowed-methods [:get]
  :available-media-types ["text/plain"]
  :authorized? (fn [_] true)
  :handle-ok "index")

(defroutes
  ^{:doc "Base application routes."}
  index-routes
  (GET "/" [] index)
  (GET "/favicon.ico" [] not-found))

(defroutes
  ^{:doc "API REST routes."}
  api-routes
  (context
    "/api"
    []
    (routes
      conditions-routes
      leaderboards-routes
      lockpick-routes
      users-routes
      clicker-routes)))

(defroutes
  ^{:doc "Collected application routes."}
  app-routes
  (routes
    api-routes
    index-routes))

(defn wrap-gnu-pratchett
  "GNU Terry Pratchett"
  [handler]
  (fn [request]
    (when-let [response (handler request)]
      (assoc-in response [:headers "X-Clacks-Overhead"] "GNU Terry Pratchett"))))

(def app
  "Entrypoint. Duh."
  (-> app-routes
      wrap-params
      wrap-keyword-params
      wrap-json-params
      wrap-edn-params
      wrap-gnu-pratchett
      (wrap-cors :access-control-allow-headers ["Origin" "X-Requested-With" "Content-Type" "Accept" "Authorization" "X-Token"]
                 :access-control-allow-methods [:get :post :options]
                 ; :access-control-allow-origin  #".*"
                 :access-control-allow-credentials "true"
                 :access-control-allow-origin  #"https?:\/\/localhost.*?\/?$")
      (wrap-trace :ui :header)))
