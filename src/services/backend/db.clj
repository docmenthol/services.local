(ns
  ^{:doc "Various database-related configuration and operations."}
  services.backend.db
  (:require [clojure.java.jdbc :as jdbc]))
            ; [clojure.string :refer [lower-case]]))

(def
  ^{:doc "Data connection configuration."}
  config
  {:store :database
   :db    {:classname   "org.sqlite.JDBC"
           :subprotocol "sqlite"
           :subname     "resources/db.sqlite3"}})

(def
  ^{:doc "Extracted database configuration."}
  db-spec
  (:db config))

; (defn- sqlize
;   [s]
;   (when s
;     (-> ^String (if (or (symbol? s) (keyword? s))
;                   (name s)
;                   (str s))
;         (.replace "-" "_")
;         lower-case)))

(defn query
  "Performs a database query given a SQL query string."
  [q]
  (vec (jdbc/query db-spec q)))

(defn insert!
  "Performs an insert query."
  [table row]
  (jdbc/insert! db-spec table row))

(defn insert-multi!
  "Performs a multiple insert query."
  [table rows]
  (jdbc/insert-multi! db-spec table rows))

(defn update!
  "Performs an update query."
  [table row constraints]
  (jdbc/update! db-spec table row constraints))

(defn delete!
  "Performs a delete query."
  [table constraints]
  (jdbc/delete! db-spec table constraints))

; (defn exists?
;   [table-key]
;   (try
;     (do
;       (->> (format "select 1 from %s" (sqlize table-key))
;            vector
;            jdbc/query)
;       true)
;     (catch Throwable ex
;       false)))
;
; (defn drop!
;   [table-key]
;   (->> (jdbc/drop-table-ddl (sqlize table-key))
;        vector
;        (jdbc/execute! db-spec))
;   db-spec)
;
; (defn drop-if-exists!
;   [table-key]
;   (when (exists? table-key)
;     (drop! table-key))
;   db-spec)
;
; (defn create!
;   [table-key & schema]
;   (try
;     (jdbc/db-do-commands db-spec (jdbc/create-table-ddl table-key schema))
;     (catch Exception e
;       (println e))))
