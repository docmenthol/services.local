(ns
  ^{:doc "Migratus integration."}
  services.backend.migrations
  (:require [migratus.core :as m]
            [services.backend.db :refer [db-spec]]))

(def
  "Migratus database configuration."
  config
  {:store :database
   :db    db-spec
   :migration-dir        "migrations/"
   :migration-table-name "services_local"})
