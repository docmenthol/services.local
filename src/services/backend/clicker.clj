(ns
  ^{:doc "Clicker game backend service."}
  services.backend.clicker
  (:require [liberator.core :refer [defresource]]
            [compojure.core :refer [defroutes POST context]]
            [services.backend.db :as db]
            [services.backend.users :refer [user-authorized-and-is?]]))

(defn get-progress-for-id
  "Returns the progress for a given user ID."
  [user-id]
  (first (db/query ["SELECT * FROM clicker WHERE user_id = ? " user-id])))

(defn update-progress-for-id
  "Updates the progress for a user."
  [user-id progress]
  (db/update! :clicker progress ["user_id = ? " user-id]))

(defresource
  ^{:doc "Player progress REST resource."}
  clicker-progress
  [{{:keys [user-id]} :params}]
  :allowed-methods [:post]
  :available-media-types ["text/html" "application/json" "application/edn"]
  ; this should actually only allow the player to call this endpoint, not any authorized user,
  ; but i'm lazy for now.
  :authorized? (user-authorized-and-is? user-id)
  :exists (fn [_]
             {::progress (get-progress-for-id user-id)})
  :handle-ok ::progress)

(defresource
  ^{:doc "Player progress REST resource."}
  update-progress
  [{{:keys [user-id progress]} :params}]
  :allowed-methods [:post]
  :available-media-types ["text/html" "application/json" "application/edn"]
  ; this should actually only allow the player to call this endpoint, not any authorized user,
  ; but i'm lazy for now.
  :authorized? (user-authorized-and-is? user-id)
  :exists (fn [ctx]
             (do (update-progress-for-id user-id progress)
                 {::progress (get-progress-for-id user-id)}))
  :handle-ok ::progress)

(defroutes
  ^{:doc "Clicker route definitions."}
  clicker-routes
  (context "/clicker" []
    (POST "/progress" [user-id]          clicker-progress)
    (POST "/update"   [user-id progress] update-progress)))
