(ns
  ^{:doc "Conditions backend service."}
  services.backend.conditions
  (:require [liberator.core :refer [defresource]]
            [compojure.core :refer [defroutes GET context]]
            [services.backend.db :as db]))

(defn get-recent-conditions
  "Returns the next 10 most recent readings after the most recent reading."
  []
  (db/query "SELECT * FROM conditions ORDER BY dt ASC LIMIT 10 OFFSET 1"))

(defn get-current-conditions
  "Returns the latest local conditions reading."
  []
  (first (db/query "SELECT * FROM conditions ORDER BY dt ASC LIMIT 1")))

(defresource
  ^{:doc "Current conditions REST resource."}
  current-conditions
  :allowed-methods [:get]
  :available-media-types ["text/html" "application/json" "application/edn"]
  :authorized? (fn [_] true)
  :handle-ok (fn [_] (get-current-conditions)))

(defresource
  ^{:doc "Recent conditions REST resource."}
  recent-conditions
  :allowed-methods [:get]
  :available-media-types ["text/html" "application/json" "application/edn"]
  :authorized? (fn [_] true)
  :handle-ok (fn [_] (get-recent-conditions)))

(defroutes
  ^{:doc "Conditions route definitions."}
  conditions-routes
  (context "/conditions" []
    (GET "/"       [] current-conditions)
    (GET "/recent" [] recent-conditions)))
