(ns
  ^{:doc "Pluggable password crypt provider."}
  services.backend.crypt
  (:require [clojurewerkz.scrypt.core :as c]))

(defn encrypt
  "Abstracted hashing function."
  [password]
  (c/encrypt password 16384 8 1))

(def verify
  "Abstracted hash comparison function."
  c/verify)
