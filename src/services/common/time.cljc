(ns
  ^{:doc "Common time functions for frontend and backend."}
  services.common.time
  (:require #?(:clj  [clj-time.coerce  :as tc]
               :cljs [cljs-time.coerce :as tc])
            #?(:clj  [clj-time.format  :as tf]
               :cljs [cljs-time.format :as tf])
            #?(:clj  [clj-time.core :as t])))

(def datetime-formatter
  "Standard datetime formatting string for the app."
  (tf/formatter "d MMM HH:mm a (YYYY)"))

(defn to-datestring
  "Converts a UNIX timestamp to a human-readable date string."
  [timestamp]
  (tf/unparse
    datetime-formatter
    (tc/from-long timestamp)))

(defn to-timestamp
  "Converts a standard datetime to a UNIX timestamp."
  [datestring]
  (-> (tf/parse datetime-formatter datestring)
      (tc/to-long)))

(defn current-timestamp
  "Returns the current time as a UNIX timestamp."
  []
  #?(:clj  (tc/to-long (t/now))
     :cljs (.now js/Date)))
