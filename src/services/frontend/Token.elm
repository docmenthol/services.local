module Token exposing (Token, decoder, encode, toString)

import Json.Decode as D
import Json.Encode as E


type Token
    = Token String


toString : Token -> String
toString (Token token) =
    token


decoder : D.Decoder Token
decoder =
    D.map Token D.string


encode : Token -> E.Value
encode (Token token) =
    E.string token
