module Viewer exposing (Viewer, cred, decoder, level, store, username)

import Api exposing (Cred)
import Json.Decode as D
import Username exposing (Username)


type Viewer
    = Viewer Cred


cred : Viewer -> Cred
cred (Viewer c) =
    c


username : Viewer -> Username
username (Viewer c) =
    Api.username c


level : Maybe Viewer -> Int
level maybeViewer =
    case maybeViewer of
        Just (Viewer c) ->
            Api.level c

        Nothing ->
            0


decoder : D.Decoder (Cred -> Viewer)
decoder =
    D.succeed Viewer


store : Viewer -> Cmd msg
store (Viewer c) =
    Api.storeCredWith c
