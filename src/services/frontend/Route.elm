module Route exposing (Route(..), fromUrl, href, parser, replaceUrl)

import Browser.Navigation as Nav
import Html exposing (Attribute)
import Html.Attributes as Attr
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>))


type Route
    = Home
    | Root
    | Login
    | Logout
    | Register
    | Profile
    | Clicker
    | Conditions
    | Scores
    | Lockpick
    | AdminGames
    | AdminPlayers
    | AdminLockpick


parser : Parser.Parser (Route -> a) a
parser =
    Parser.oneOf
        [ Parser.map Home Parser.top
        , Parser.map Login (Parser.s "login")
        , Parser.map Logout (Parser.s "logout")
        , Parser.map Register (Parser.s "register")
        , Parser.map Profile (Parser.s "profile")
        , Parser.map Clicker (Parser.s "clicker")
        , Parser.map Conditions (Parser.s "conditions")
        , Parser.map Scores (Parser.s "leaderboards")
        , Parser.map Lockpick (Parser.s "lockpick")
        , Parser.map AdminGames (Parser.s "admin" </> Parser.s "games")
        , Parser.map AdminPlayers (Parser.s "admin" </> Parser.s "players")
        , Parser.map AdminLockpick (Parser.s "admin" </> Parser.s "lockpick")
        ]


href : Route -> Attribute msg
href targetRoute =
    Attr.href <| routeToString targetRoute


replaceUrl : Nav.Key -> Route -> Cmd msg
replaceUrl key route =
    Nav.replaceUrl key <| routeToString route


fromUrl : Url -> Maybe Route
fromUrl url =
    { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing }
        |> Parser.parse parser


routeToString : Route -> String
routeToString route =
    "#/" ++ String.join "/" (routeToPieces route)


routeToPieces : Route -> List String
routeToPieces route =
    case route of
        Home ->
            []

        Root ->
            []

        Login ->
            [ "login" ]

        Logout ->
            [ "logout" ]

        Register ->
            [ "register" ]

        Profile ->
            [ "profile" ]

        Clicker ->
            [ "clicker" ]

        Conditions ->
            [ "conditions" ]

        Scores ->
            [ "leaderboards" ]

        Lockpick ->
            [ "lockpick" ]

        AdminGames ->
            [ "admin", "games" ]

        AdminPlayers ->
            [ "admin", "players" ]

        AdminLockpick ->
            [ "admin", "lockpick" ]
