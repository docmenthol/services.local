module Api.Profile exposing (Profile(..), UserProfile, decoder)

import Json.Decode as D
import Json.Decode.Pipeline exposing (required)


type alias UserProfile =
    { id : Int
    , userID : Int
    , playerID : Maybe Int
    , avatar : String
    }


type Profile
    = Profile UserProfile
    | EmptyProfile


decoder : D.Decoder UserProfile
decoder =
    D.succeed UserProfile
        |> required "id" D.int
        |> required "user_id" D.int
        |> required "player_id" (D.maybe D.int)
        |> required "avatar" D.string
