module Api.User exposing (User, decoder, toString)

import Json.Decode as D
import Json.Decode.Pipeline exposing (required)


type alias User =
    { id : Int
    , username : String
    }


decoder : D.Decoder User
decoder =
    D.succeed User
        |> required "id" D.int
        |> required "username" D.string


toString : User -> String
toString user =
    user.username
