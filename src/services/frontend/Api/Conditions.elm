module Api.Conditions exposing (Condition, decoder)

import Json.Decode as D
import Json.Decode.Pipeline exposing (required)


type alias Condition =
    { dt : Int
    , temperature : Float
    , lumens : Int
    }


decoder : D.Decoder Condition
decoder =
    D.succeed Condition
        |> required "dt" D.int
        |> required "temperature" D.float
        |> required "lumens" D.int
