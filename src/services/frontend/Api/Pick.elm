module Api.Pick exposing (Pick, decoder, toString)

import Json.Decode as D
import Json.Decode.Pipeline exposing (required)


type alias Pick =
    { lockID : Int
    , pickID : Int
    , lockName : String
    , pickName : String
    }


decoder : D.Decoder Pick
decoder =
    D.succeed Pick
        |> required "lock_id" D.int
        |> required "user_id" D.int
        |> required "lock_name" D.string
        |> required "pick_name" D.string


toString : Pick -> String
toString pick =
    pick.pickName ++ ", " ++ pick.lockName
