module Api.LockLevel exposing (LockLevel(..), decodeLevel, decoder, toString)

import Json.Decode as D


type LockLevel
    = Training
    | White
    | Yellow
    | Orange
    | Green
    | Blue
    | Purple
    | Brown
    | Black
    | Red


decodeLevel : Int -> D.Decoder LockLevel
decodeLevel level =
    case level of
        0 ->
            D.succeed Training

        1 ->
            D.succeed White

        2 ->
            D.succeed Yellow

        3 ->
            D.succeed Orange

        4 ->
            D.succeed Green

        5 ->
            D.succeed Blue

        6 ->
            D.succeed Purple

        7 ->
            D.succeed Brown

        8 ->
            D.succeed Black

        9 ->
            D.succeed Red

        _ ->
            D.fail "Unknown level"


toInt : LockLevel -> Int
toInt level =
    case level of
        Training ->
            0

        White ->
            1

        Yellow ->
            2

        Orange ->
            3

        Green ->
            4

        Blue ->
            5

        Purple ->
            6

        Brown ->
            7

        Black ->
            8

        Red ->
            9


toString : LockLevel -> String
toString level =
    let
        name =
            case level of
                Training ->
                    "Training"

                White ->
                    "White"

                Yellow ->
                    "Yellow"

                Orange ->
                    "Orange"

                Green ->
                    "Green"

                Blue ->
                    "Blue"

                Purple ->
                    "Purple"

                Brown ->
                    "Brown"

                Black ->
                    "Black"

                Red ->
                    "Red"

        levelNum =
            String.fromInt <| toInt level
    in
    name ++ " (" ++ levelNum ++ ")"


decoder : D.Decoder LockLevel
decoder =
    D.int
        |> D.andThen decodeLevel
