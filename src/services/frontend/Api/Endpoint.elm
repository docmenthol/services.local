module Api.Endpoint exposing
    ( Endpoint
    , conditions
    , createGame
    , createPlayer
    , games
    , lockpick
    , locks
    , login
    , logout
    , picks
    , players
    , recentConditions
    , register
    , request
    , scores
    , scoresForGame
    , scoresForPlayer
    , singleGame
    , singlePlayer
    , updateGame
    , updatePlayer
    , users
    )

import Http
import Url.Builder exposing (crossOrigin)


request :
    { body : Http.Body
    , expect : Http.Expect msg
    , headers : List Http.Header
    , method : String
    , timeout : Maybe Float
    , url : Endpoint
    , withCredentials : Bool
    }
    -> Cmd msg
request config =
    Http.riskyRequest
        { body = config.body
        , expect = config.expect
        , headers = Http.header "Accept" "application/json" :: config.headers
        , method = config.method
        , timeout = config.timeout
        , url = unwrap config.url
        , tracker = Nothing
        }


type Endpoint
    = Endpoint String


unwrap : Endpoint -> String
unwrap (Endpoint str) =
    str


url : List String -> Endpoint
url path =
    let
        fullEndpoint =
            "api" :: path
    in
    Endpoint <| crossOrigin "http://localhost:3000" fullEndpoint []



-- Roots


leaderboards : List String -> Endpoint
leaderboards path =
    url <| "leaderboards" :: path


lockpick : List String -> Endpoint
lockpick path =
    url <| "lockpick" :: path


users : List String -> Endpoint
users path =
    url <| "users" :: path



-- Account Endpoints


login : Endpoint
login =
    url [ "login" ]


logout : Endpoint
logout =
    url [ "logout" ]


register : Endpoint
register =
    url [ "register" ]



-- Conditions Routes


conditions : Endpoint
conditions =
    url [ "conditions" ]


recentConditions : Endpoint
recentConditions =
    url [ "conditions", "recent" ]



-- Leaderboard Routes


players : List String -> Endpoint
players path =
    leaderboards <| "players" :: path


singlePlayer : Int -> Endpoint
singlePlayer id =
    players [ String.fromInt id ]


createPlayer : Endpoint
createPlayer =
    players [ "create" ]


updatePlayer : Int -> Endpoint
updatePlayer id =
    players [ "update", String.fromInt id ]


games : List String -> Endpoint
games path =
    leaderboards <| "games" :: path


singleGame : Int -> Endpoint
singleGame id =
    games [ String.fromInt id ]


createGame : Endpoint
createGame =
    games [ "create" ]


updateGame : Int -> Endpoint
updateGame id =
    games [ "update", String.fromInt id ]


scores : List String -> Endpoint
scores path =
    leaderboards <| "scores" :: path


scoresForPlayer : Int -> Endpoint
scoresForPlayer playerID =
    scores [ "player", String.fromInt playerID ]


scoresForGame : Int -> Endpoint
scoresForGame gameID =
    scores [ "game", String.fromInt gameID ]



-- Lockpick Routes


locks : List String -> Endpoint
locks path =
    lockpick <| "locks" :: path


picks : List String -> Endpoint
picks path =
    lockpick <| "picks" :: path
