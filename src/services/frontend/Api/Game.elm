module Api.Game exposing (Game, decoder, encode, equal, toString)

import Json.Decode as D
import Json.Decode.Pipeline exposing (required)
import Json.Encode as E


type alias Game =
    { id : Int
    , name : String
    , units : String
    , prefix : Int
    }


decoder : D.Decoder Game
decoder =
    D.succeed Game
        |> required "id" D.int
        |> required "name" D.string
        |> required "units" D.string
        |> required "prefix" D.int


encode : Game -> E.Value
encode game =
    E.object
        [ ( "id", E.int game.id )
        , ( "name", E.string game.name )
        , ( "units", E.string game.units )
        , ( "prefix", E.int game.prefix )
        ]


toString : Game -> String
toString game =
    game.name


equal : Game -> Game -> Bool
equal g h =
    g.id == h.id && g.name == h.name && g.units == h.units && g.prefix == h.prefix
