module Api.Login exposing (Login, decoder)

import Json.Decode as D
import Json.Decode.Pipeline exposing (optional, required)


type alias Login =
    { username : String
    , level : Int
    , token : String
    }


decoder : D.Decoder Login
decoder =
    D.succeed Login
        |> required "username" D.string
        |> optional "level" D.int 0
        |> required "token" D.string
