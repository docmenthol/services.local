module Api.Score exposing (Score, decoder, toString)

import Json.Decode as D
import Json.Decode.Pipeline exposing (required)


type alias Score =
    { id : Int
    , score : Float
    , gameID : Int
    , playerID : Int
    , gameName : String
    , playerName : String
    , prefix : Bool
    , units : String
    , adjacent : Bool
    }


decoder : D.Decoder Score
decoder =
    D.succeed Score
        |> required "id" D.int
        |> required "score" D.float
        |> required "game_id" D.int
        |> required "player_id" D.int
        |> required "game_name" D.string
        |> required "player_name" D.string
        |> required "prefix" D.bool
        |> required "units" D.string
        |> required "adjacent" D.bool


toString : Score -> String
toString s =
    let
        score =
            String.fromFloat s.score
    in
    case ( s.prefix, s.adjacent ) of
        ( True, True ) ->
            s.units ++ score

        ( True, False ) ->
            s.units ++ " " ++ score

        ( False, False ) ->
            score ++ " " ++ s.units

        ( False, True ) ->
            score ++ s.units
