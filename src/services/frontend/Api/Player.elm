module Api.Player exposing (Player, decoder, equal, toString)

import Json.Decode as D
import Json.Decode.Pipeline exposing (optional, required)


type alias Player =
    { id : Int
    , name : String
    , userID : Int
    , username : String
    }


decoder : D.Decoder Player
decoder =
    D.succeed Player
        |> required "id" D.int
        |> required "name" D.string
        |> optional "user_id" D.int 0
        |> optional "username" D.string "(None)"


toString : Player -> String
toString player =
    player.name


equal : Player -> Player -> Bool
equal p q =
    p.id == q.id && p.name == q.name && p.userID == q.userID
