module Api.Lock exposing (Lock, decoder, sort, toString)

import Api.LockLevel as LockLevel exposing (LockLevel)
import Json.Decode as D
import Json.Decode.Pipeline exposing (required)


type alias Lock =
    { id : Int
    , name : String
    , level : LockLevel
    }


decoder : D.Decoder Lock
decoder =
    D.succeed Lock
        |> required "id" D.int
        |> required "name" D.string
        |> required "level" LockLevel.decoder


toString : Lock -> String
toString lock =
    lock.name


sort : List Lock -> List Lock
sort locks =
    List.sortBy .id locks
