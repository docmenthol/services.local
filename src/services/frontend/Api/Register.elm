module Api.Register exposing (Register, decoder)

import Json.Decode as D
import Json.Decode.Pipeline exposing (optional, required)


type alias Register =
    { id : Int
    , username : String
    , level : Int
    , api_key : String
    }


decoder : D.Decoder Register
decoder =
    D.succeed Register
        |> required "id" D.int
        |> required "username" D.string
        |> required "level" D.int
        |> optional "api_key" D.string ""
