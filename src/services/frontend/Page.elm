module Page exposing (Page(..), divider, view)

import Api exposing (Cred(..))
import Browser exposing (Document)
import Html exposing (Html, a, div, main_, nav, span, text)
import Html.Attributes as HA exposing (class, href)
import Route
import Svg exposing (circle, path, svg)
import Svg.Attributes as SA
import Username
import Viewer exposing (Viewer)


type Page
    = Home
    | Other
    | Login
    | Register
    | Profile
    | Clicker
    | Conditions
    | Scores
    | Lockpick
    | AdminGames
    | AdminPlayers
    | AdminLockpick


divider : Html msg
divider =
    svg [ SA.class "w-full pt-5 pb-12 fill-current", SA.viewBox "0 0 1024 9", SA.opacity "0.75" ]
        [ circle [ SA.cx "4", SA.cy "4", SA.r "3", SA.stroke "white", SA.fill "#2d3748" ] []
        , circle [ SA.cx "1020", SA.cy "4", SA.r "3", SA.stroke "white", SA.fill "#2d3748" ] []
        , path [ SA.d "M7,4 l1010,0", SA.stroke "white" ] []
        ]


verticalDivider : Html msg
verticalDivider =
    div []
        [ svg
            [ SA.class "fill-current h-10 w-6 mr-4", SA.viewBox "0 0 10 50" ]
            [ path [ SA.d "M4,0 L1,0 L1,50 L0,50 L0,0" ] [] ]
        ]


view : Maybe Viewer -> Page -> { title : String, content : Html msg } -> Document msg
view maybeViewer page { title, content } =
    { title = title ++ " :: services.local"
    , body =
        [ viewHeader page maybeViewer
        , viewContent content
        ]
    }


viewHeader : Page -> Maybe Viewer -> Html msg
viewHeader _ maybeViewer =
    nav
        [ HA.class "nav" ]
        [ div [ HA.class "heading__logo" ]
            [ a [ HA.class "heading__text hover:text-gray-300", Route.href Route.Home ]
                [ text "services.local" ]
            ]
        , verticalDivider
        , div [ HA.class "nav__menu" ]
            [ div [ HA.class "text-sm lg:flex-grow" ] <|
                List.concat
                    [ viewPublicPages
                    , viewLoggedInPages maybeViewer
                    , viewHigherPages maybeViewer
                    , viewEvenHigherPages maybeViewer
                    ]
            , case maybeViewer of
                Just viewer ->
                    let
                        username =
                            Username.toString <| Viewer.username viewer
                    in
                    div [ HA.class "inline-flex items-center" ]
                        [ span [ HA.class "mr-4" ] [ text <| "hello, " ++ username ]
                        , a [ HA.class "button rounded-l rounded-r-none", Route.href Route.Profile ] [ text "Profile" ]
                        , a [ HA.class "button rounded-r rounded-l-none", Route.href Route.Logout ] [ text "Logout" ]
                        ]

                Nothing ->
                    div [ HA.class "inline-flex" ]
                        [ a [ HA.class "button rounded-l rounded-r-none", Route.href Route.Login ] [ text "Login" ]
                        , a [ HA.class "button rounded-r rounded-l-none", Route.href Route.Register ] [ text "Register" ]
                        ]
            ]
        ]


viewContent : Html msg -> Html msg
viewContent content =
    main_ [ HA.class "main__container" ] [ content ]


viewPublicPages : List (Html msg)
viewPublicPages =
    [ a [ HA.class "nav__item", Route.href Route.Conditions ] [ text "conditions" ]
    , a [ HA.class "nav__item", Route.href Route.Scores ] [ text "leaderboards" ]
    ]


viewLoggedInPages : Maybe Viewer -> List (Html msg)
viewLoggedInPages maybeViewer =
    case maybeViewer of
        Just _ ->
            [ span [ class "pr-3" ] [ text "⎔" ]
            , a [ HA.class "nav__item", Route.href Route.Lockpick ] [ text "lockpick" ]

            --, a [ HA.class "nav__item", Route.href Route.Clicker ] [ text "clicker" ]
            ]

        Nothing ->
            []


viewHigherPages : Maybe Viewer -> List (Html msg)
viewHigherPages maybeViewer =
    if Viewer.level maybeViewer >= 2 then
        [ span [ class "pr-3" ] [ text "⎔" ]
        , a [ HA.class "nav__item", Route.href Route.AdminGames ] [ text "games" ]
        , a [ HA.class "nav__item", Route.href Route.AdminPlayers ] [ text "players" ]
        , a [ HA.class "nav__item", Route.href Route.AdminLockpick ] [ text "locks & picks" ]
        ]

    else
        []


viewEvenHigherPages : Maybe Viewer -> List (Html msg)
viewEvenHigherPages maybeViewer =
    if Viewer.level maybeViewer >= 3 then
        [ span [ class "pr-3" ] [ text "⎔" ]
        , span [ HA.class "nav__item" ] [ text "users" ]
        ]

    else
        []
