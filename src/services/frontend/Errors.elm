module Errors exposing (toString)

import Http exposing (Error(..))


fromStatus : Int -> String
fromStatus status =
    let
        msg =
            case status of
                401 ->
                    "Unauthorized."

                _ ->
                    "Unknown bad status code."
    in
    String.fromInt status ++ ": " ++ msg


toString : Error -> String
toString error =
    case error of
        BadStatus status ->
            fromStatus status

        BadBody err ->
            err

        NetworkError ->
            "Unknown network error."

        Timeout ->
            "Connection timed out."

        _ ->
            "Unknown error."
