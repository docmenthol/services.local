module Page.NotFound exposing (Model, Msg(..), subscriptions, toSession, update, view)

import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Route
import Session exposing (Session)


type alias Model =
    Session


type Msg
    = GotSession Session


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg _ =
    case msg of
        GotSession session ->
            ( session
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


view : { title : String, content : Html msg }
view =
    { title = "Page Not Found"
    , content =
        div [ class "content-tiny mt-20" ] [ text "Page not found." ]
    }


toSession : Model -> Session
toSession =
    identity
