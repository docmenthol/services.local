module Page.Register exposing (Model, Msg(..), init, subscriptions, toSession, update, view)

import Api
import Api.Endpoint exposing (register)
import Api.Register as Register exposing (Register)
import Errors
import Html exposing (Html, button, div, form, input, label, text)
import Html.Attributes exposing (class, for, name, type_)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Encode as E
import LoadingState exposing (LoadingState(..))
import Route
import Session exposing (Session)
import Viewer exposing (Viewer)


type alias Model =
    { username : String
    , password : String
    , passwordAgain : String
    , spam : String
    , registering : LoadingState Http.Error ()
    , loggingIn : LoadingState Http.Error ()
    , session : Session
    }


type Msg
    = ChangeUser String
    | ChangePassword String
    | ChangePasswordAgain String
    | ChangeSpam String
    | SendRegister
    | RegisterResult (Result Http.Error Register)
    | LoginResult (Result Http.Error Viewer)
    | GotSession Session


encodeRegister : Model -> E.Value
encodeRegister v =
    E.object
        [ ( "username", E.string v.username )
        , ( "password", E.string v.password )
        , ( "spam", E.string "" )
        ]


encodeLogin : Model -> E.Value
encodeLogin v =
    E.object
        [ ( "username", E.string v.username )
        , ( "password", E.string v.password )
        ]


init : Session -> ( Model, Cmd Msg )
init session =
    ( { username = ""
      , password = ""
      , passwordAgain = ""
      , spam = ""
      , registering = NotLoading
      , loggingIn = NotLoading
      , session = session
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model.session)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeUser username ->
            ( { model | username = username }, Cmd.none )

        ChangePassword password ->
            ( { model | password = password }, Cmd.none )

        ChangePasswordAgain password ->
            ( { model | passwordAgain = password }, Cmd.none )

        ChangeSpam spam ->
            ( { model | spam = spam }, Cmd.none )

        SendRegister ->
            let
                body =
                    Http.jsonBody <| encodeRegister model
            in
            ( model
            , Api.post register Nothing body RegisterResult Register.decoder
            )

        RegisterResult result ->
            case result of
                Ok _ ->
                    let
                        body =
                            Http.jsonBody <| encodeLogin model
                    in
                    ( { model | registering = Loaded () }
                    , Api.login body LoginResult Viewer.decoder
                    )

                Err err ->
                    ( { model | registering = Failed err }, Cmd.none )

        LoginResult result ->
            case result of
                Ok viewer ->
                    ( { model | loggingIn = Loaded () }, Viewer.store viewer )

                Err err ->
                    ( { model | loggingIn = Failed err }, Cmd.none )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


valid : Model -> Bool
valid model =
    String.length model.username > 0 && String.length model.password > 0 && model.password == model.passwordAgain


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "login"
    , content =
        div [ class "content-tiny" ]
            [ div [ class "w-full max-w-ws" ]
                [ form [ class "form-mini" ]
                    [ viewField model "username" "text" ChangeUser
                    , viewField model "password" "password" ChangePassword
                    , viewField model "password (confirm)" "password" ChangePasswordAgain
                    , div [ class "mb-4 form__input-email" ]
                        [ label [ class "form__label", for "email" ] [ text "email" ]
                        , input [ class "form__input-text", name "email", type_ "text", onInput ChangeSpam ] [ text model.spam ]
                        ]
                    , div [ class "flex items-center justify-between" ] <|
                        List.concat
                            [ [ button
                                    [ if valid model then
                                        class "button-light rounded"

                                      else
                                        class "button-light button-disabled rounded"
                                    , type_ "button"
                                    , onClick SendRegister
                                    ]
                                    [ text "Register" ]
                              ]
                            , case model.registering of
                                Failed err ->
                                    [ div [] [ text <| "Registration failed: " ++ Errors.toString err ] ]

                                Loading ->
                                    [ div [] [ text "Sending registration..." ] ]

                                _ ->
                                    []
                            , case model.loggingIn of
                                Failed err ->
                                    [ div [] [ text <| "Automatic login failed: " ++ Errors.toString err ] ]

                                Loading ->
                                    [ div [] [ text "Logging in..." ] ]

                                _ ->
                                    []
                            ]
                    ]
                ]
            ]
    }


viewField : Model -> String -> String -> (String -> Msg) -> Html Msg
viewField model inputName inputType changeEvent =
    div [ class "mb-4" ]
        [ label [ class "form__label", for inputName ] [ text inputName ]
        , input [ class "form__input-text", name inputName, type_ inputType, onInput changeEvent ] [ text model.username ]
        ]


toSession : Model -> Session
toSession model =
    model.session
