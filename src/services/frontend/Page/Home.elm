module Page.Home exposing (Model, Msg(..), init, subscriptions, toSession, update, view)

import Html exposing (Html, div, h2, text)
import Html.Attributes exposing (class)
import Page exposing (divider)
import Route
import Session exposing (Session)


type alias Model =
    Session


type Msg
    = GotSession Session


init : Session -> ( Model, Cmd Msg )
init session =
    ( session, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg _ =
    case msg of
        GotSession session ->
            ( session
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model)


view : Model -> { title : String, content : Html Msg }
view _ =
    { title = "welcome"
    , content =
        div [ class "content-slim text-center" ]
            [ h2 [ class "heading object-center py-4 block" ] [ text "services.local" ]
            , divider
            ]
    }


toSession : Model -> Session
toSession =
    identity
