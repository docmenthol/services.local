module Page.Scores exposing
    ( Model
    , Msg(..)
    , init
    , subscriptions
    , toSession
    , update
    , view
    )

import Api
import Api.Endpoint as Endpoint
import Api.Game as Game exposing (Game)
import Api.Player as Player exposing (Player)
import Api.Score as Score exposing (Score)
import Autotable as AT
import Autotable.Options as ATO
import Errors
import Html exposing (Html, div, h2, label, option, select, text)
import Html.Attributes exposing (class, for, value)
import Html.Events exposing (onClick)
import Http
import Json.Decode as D
import LoadingState exposing (LoadingState(..))
import Page exposing (divider)
import Route
import Session exposing (Session)
import Svg exposing (path, svg)
import Svg.Attributes as SA


type alias Model =
    { games : LoadingState Http.Error (List Game)
    , players : LoadingState Http.Error (List Player)
    , gameScores : LoadingState Http.Error (AT.Model Msg Score)
    , playerScores : LoadingState Http.Error (AT.Model Msg Score)
    , playerFilter : Maybe Int
    , gameFilter : Maybe Int
    , session : Session
    }


type Msg
    = GotGames (Result Http.Error (List Game))
    | GotPlayers (Result Http.Error (List Player))
    | GotGameScores (Result Http.Error (List Score))
    | GotPlayerScores (Result Http.Error (List Score))
    | PlayerFilter String
    | GameFilter String
    | GotSession Session
    | GameTableMsg AT.Msg
    | PlayerTableMsg AT.Msg


tableOptions : ATO.Options
tableOptions =
    ATO.Options ATO.Sorting ATO.Filtering ATO.NoSelecting ATO.NoDragging ATO.NoEditing (ATO.Pagination 10) ATO.NoFill


playerScoreColumns : List (AT.Column Msg Score)
playerScoreColumns =
    [ AT.Column
        "Game"
        "game"
        .gameName
        (\r _ -> text <| r.gameName)
        .gameName
        (\r f -> String.startsWith (String.toLower f) (String.toLower r.gameName))
        (\r _ -> r)
    , AT.Column
        "Score"
        "score"
        Score.toString
        (\r _ -> text <| Score.toString r)
        Score.toString
        (\_ _ -> True)
        (\r _ -> r)
    ]


gameScoreColumns : List (AT.Column Msg Score)
gameScoreColumns =
    [ AT.Column
        "Player"
        "player"
        .playerName
        (\r _ -> text <| r.playerName)
        .playerName
        (\r f -> String.startsWith (String.toLower f) (String.toLower r.playerName))
        (\r _ -> r)
    , AT.Column
        "Score"
        "score"
        Score.toString
        (\r _ -> text <| Score.toString r)
        Score.toString
        (\_ _ -> True)
        (\r _ -> r)
    ]



--getPlayer : List Player -> Int -> Player
--getPlayer players id =
--    -- This is bad. Probably should just use Maybe.
--    let
--        s =
--            List.head <| List.filter (\p -> p.id == id) players
--    in
--    Maybe.withDefault (Player 0 "" 0 "") s
--getGame : List Game -> Int -> Game
--getGame games id =
--    let
--        s =
--            List.head <| List.filter (\g -> g.id == id) games
--    in
--    Maybe.withDefault (Game 0 "" "" 0) s


init : Session -> ( Model, Cmd Msg )
init session =
    let
        players =
            Endpoint.players []

        games =
            Endpoint.games []
    in
    ( { games = Loading, players = Loading, gameScores = NotLoading, playerScores = NotLoading, playerFilter = Nothing, gameFilter = Nothing, session = session }
    , Cmd.batch
        [ Api.get players Nothing GotPlayers <| D.list Player.decoder
        , Api.get games Nothing GotGames <| D.list Game.decoder
        ]
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model.session)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotPlayers result ->
            case result of
                Ok players ->
                    ( { model | players = Loaded players }, Cmd.none )

                Err err ->
                    ( { model | players = Failed err }, Cmd.none )

        GotGames result ->
            case result of
                Ok games ->
                    ( { model | games = Loaded games }, Cmd.none )

                Err err ->
                    ( { model | games = Failed err }, Cmd.none )

        GotPlayerScores result ->
            case result of
                Ok scores ->
                    ( { model | playerScores = Loaded <| AT.init "player-scores" playerScoreColumns scores tableOptions }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        GotGameScores result ->
            case result of
                Ok scores ->
                    ( { model | gameScores = Loaded <| AT.init "game-scores" gameScoreColumns scores tableOptions }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        PlayerFilter filter ->
            let
                endpoint =
                    Endpoint.scores [ "player", filter ]
            in
            ( { model | playerFilter = String.toInt filter }
            , Api.get endpoint Nothing GotPlayerScores <| D.list Score.decoder
            )

        GameFilter filter ->
            let
                endpoint =
                    Endpoint.scores [ "game", filter ]
            in
            ( { model | gameFilter = String.toInt filter }
            , Api.get endpoint Nothing GotGameScores <| D.list Score.decoder
            )

        PlayerTableMsg subMsg ->
            let
                playerScores =
                    LoadingState.map model.playerScores <| AT.update subMsg
            in
            ( { model | playerScores = playerScores }
            , Cmd.none
            )

        GameTableMsg subMsg ->
            let
                gameScores =
                    LoadingState.map model.gameScores <| AT.update subMsg
            in
            ( { model | gameScores = gameScores }
            , Cmd.none
            )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "leaderboards"
    , content =
        div [ class "content-slim text-center" ]
            [ h2 [ class "heading object-center py-4 block" ] [ text "leaderboards" ]
            , divider
            , div [ class "w-full flex flex-wrap mb-4" ]
                [ viewPlayerSelect model
                , viewGameSelect model
                , viewPlayerScores model
                , viewGameScores model
                ]
            ]
    }


viewPlayerSelect : Model -> Html Msg
viewPlayerSelect model =
    let
        filterValue =
            case model.playerFilter of
                Just int ->
                    String.fromInt int

                Nothing ->
                    ""
    in
    case model.players of
        Loaded players ->
            div [ class "mb-6 px-2 relative w-1/2" ]
                [ label [ class "form__label", for "player" ] [ text "select player" ]
                , select [ class "form__select", value filterValue ]
                    (List.map
                        (\p ->
                            let
                                id =
                                    String.fromInt p.id
                            in
                            option [ value id, onClick <| PlayerFilter id ] [ text p.name ]
                        )
                        players
                    )
                , div
                    [ class "form__select-arrow pt-6 pr-6" ]
                    [ svg
                        [ SA.class "fill-current h-4 w-4", SA.viewBox "0 0 20 20" ]
                        [ path [ SA.d "M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z", SA.fill "white" ] [] ]
                    ]
                ]

        Loading ->
            div [ class "mb-6 px-2 w-1/2 text-center" ] [ text "Loading..." ]

        NotLoading ->
            div [ class "mb-6 px-2 w-1/2 text-center" ] [ text "Idle." ]

        Failed err ->
            div [ class "mb-6 px-2 w-1/2 text-center" ] [ text <| Errors.toString err ]


viewPlayerScores : Model -> Html Msg
viewPlayerScores model =
    case model.playerScores of
        NotLoading ->
            case model.players of
                Loaded _ ->
                    div [ class "w-1/2 px-2" ] [ text "Select a player." ]

                _ ->
                    div [] []

        Loading ->
            div [ class "w-1/2 px-2" ] [ text "Loading scores..." ]

        Loaded scores ->
            div [ class "w-1/2 px-2" ] [ AT.view scores PlayerTableMsg ]

        Failed _ ->
            div [ class "w-1/2 px-2" ] [ text "Error loading scores." ]


viewGameSelect : Model -> Html Msg
viewGameSelect model =
    let
        filterValue =
            case model.gameFilter of
                Just int ->
                    String.fromInt int

                Nothing ->
                    ""
    in
    case model.games of
        Loaded games ->
            div
                [ class "mb-6 px-2 relative w-1/2" ]
                [ label [ class "form__label", for "game" ] [ text "select game" ]
                , select
                    [ class "form__select", value filterValue ]
                  <|
                    List.map
                        (\g ->
                            let
                                id =
                                    String.fromInt g.id
                            in
                            option [ value id, onClick <| GameFilter id ] [ text g.name ]
                        )
                        games
                , div
                    [ class "form__select-arrow pt-6 pr-6" ]
                    [ svg
                        [ SA.class "fill-current h-4 w-4", SA.viewBox "0 0 20 20" ]
                        [ path [ SA.d "M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z", SA.fill "white" ] [] ]
                    ]
                ]

        Loading ->
            div [ class "mb-6 px-2 w-1/2 text-center" ] [ text "Loading..." ]

        NotLoading ->
            div [ class "mb-6 px-2 w-1/2 text-center" ] [ text "Idle." ]

        Failed err ->
            div [ class "mb-6 px-2 w-1/2 text-center" ] [ text <| Errors.toString err ]


viewGameScores : Model -> Html Msg
viewGameScores model =
    case model.gameScores of
        NotLoading ->
            case model.games of
                Loaded _ ->
                    div [ class "w-1/2 px-2" ] [ text "Select a game." ]

                _ ->
                    div [] []

        Loading ->
            div [ class "w-1/2 px-2" ] [ text "Loading scores..." ]

        Loaded scores ->
            div [ class "w-1/2 px-2" ] [ AT.view scores GameTableMsg ]

        Failed _ ->
            div [ class "w-1/2 px-2" ] [ text "Error loading scores." ]


toSession : Model -> Session
toSession model =
    model.session
