module Page.Clicker.Particle exposing (Particle(..), all, cost, produce, toHtml, toString)

import Html exposing (Html, span, sup, text)
import Page.Clicker.Production as Production exposing (Production)


type Particle
    = Quark
    | Neutron
    | Proton
    | Electron
    | Hydrogen
    | Helium


all : List Particle
all =
    [ Quark
    , Neutron
    , Proton
    , Electron
    , Hydrogen
    , Helium
    ]


cost : Particle -> Production
cost particle =
    case particle of
        Quark ->
            Production.empty

        Neutron ->
            Production.quarks 3.0

        Proton ->
            Production.quarks 3.0

        Electron ->
            Production.quarks 3.0

        Hydrogen ->
            Production.sum
                [ Production.neutrons 1
                , Production.electrons 1
                , Production.protons 1
                ]

        Helium ->
            Production.sum
                [ Production.neutrons 2
                , Production.electrons 2
                , Production.protons 2
                ]


produce : Particle -> Production
produce particle =
    case particle of
        Quark ->
            Production.quarks 1.0

        Neutron ->
            Production.neutrons 1.0

        Proton ->
            Production.protons 1.0

        Electron ->
            Production.electrons 1.0

        Hydrogen ->
            Production.hydrogen 1.0

        Helium ->
            Production.helium 1.0


toString : Particle -> String
toString particle =
    case particle of
        Quark ->
            "Quarks"

        Neutron ->
            "Neutrons"

        Proton ->
            "Protons"

        Electron ->
            "Electrons"

        Hydrogen ->
            "Hydrogen Atoms"

        Helium ->
            "Helium Atoms"


toHtml : Particle -> Html msg
toHtml particle =
    case particle of
        Quark ->
            span [] [ text "q" ]

        Neutron ->
            span [] [ text "n", sup [] [ text "0" ] ]

        Proton ->
            span [] [ text "p", sup [] [ text "+" ] ]

        Electron ->
            span [] [ text "β", sup [] [ text "-" ] ]

        Hydrogen ->
            span [] [ text "H" ]

        Helium ->
            span [] [ text "He" ]
