module Page.Clicker.Building exposing (Building(..), all, calculateCost, productionFor)

import Page.Clicker.Production as Production exposing (Production)


type Building
    = QuarkCollector
    | NeutronCollector
    | ProtonCollector
    | ElectronCollector
    | HydrogenCollector
    | HeliumCollector


all : List Building
all =
    [ QuarkCollector
    , NeutronCollector
    , ProtonCollector
    , ElectronCollector
    , HydrogenCollector
    , HeliumCollector
    ]


calculateCost : Building -> Int -> Production
calculateCost building level =
    let
        base =
            cost building
    in
    Production.scale (toFloat level) base


atomicBase : Int -> Production
atomicBase scale =
    let
        s =
            toFloat scale
    in
    Production.scale s <|
        Production.sum
            [ Production.neutrons -0.03
            , Production.electrons -0.03
            , Production.protons -0.03
            ]


productionFor : Building -> Production
productionFor building =
    --let
    --    base =
    --        Production.empty
    --in
    case building of
        QuarkCollector ->
            Production.quarks 0.1

        NeutronCollector ->
            Production.sum [ Production.neutrons 0.01, Production.quarks -0.03 ]

        ProtonCollector ->
            Production.sum [ Production.protons 0.01, Production.quarks -0.03 ]

        ElectronCollector ->
            Production.sum [ Production.electrons 0.01, Production.quarks -0.03 ]

        HydrogenCollector ->
            Production.sum
                [ Production.hydrogen 0.01
                , atomicBase 1
                ]

        HeliumCollector ->
            Production.sum
                [ Production.helium 0.01
                , atomicBase 2
                ]


cost : Building -> Production
cost building =
    case building of
        QuarkCollector ->
            Production.quarks 100

        NeutronCollector ->
            Production.sum
                [ Production.quarks 200
                , Production.neutrons 10
                , Production.protons 10
                , Production.electrons 10
                ]

        ProtonCollector ->
            Production.sum
                [ Production.quarks 200
                , Production.neutrons 10
                , Production.protons 10
                , Production.electrons 10
                ]

        ElectronCollector ->
            Production.sum
                [ Production.quarks 200
                , Production.neutrons 10
                , Production.protons 10
                , Production.electrons 10
                ]

        HydrogenCollector ->
            Production.hydrogen 100

        HeliumCollector ->
            Production.helium 100
