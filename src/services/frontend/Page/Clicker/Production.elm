module Page.Clicker.Production exposing
    ( Production
    , combine
    , electrons
    , empty
    , has
    , helium
    , hydrogen
    , neutrons
    , protons
    , quarks
    , scale
    , sum
    , valid
    )


type alias Production =
    { quarks : Float
    , neutrons : Float
    , protons : Float
    , electrons : Float
    , hydrogen : Float
    , helium : Float
    }


empty : Production
empty =
    { quarks = 0.0
    , neutrons = 0.0
    , protons = 0.0
    , electrons = 0.0
    , hydrogen = 0.0
    , helium = 0.0
    }


quarks : Float -> Production
quarks a =
    { empty | quarks = a }


neutrons : Float -> Production
neutrons a =
    { empty | neutrons = a }


protons : Float -> Production
protons a =
    { empty | protons = a }


electrons : Float -> Production
electrons a =
    { empty | electrons = a }


hydrogen : Float -> Production
hydrogen a =
    { empty | hydrogen = a }


helium : Float -> Production
helium a =
    { empty | helium = a }


scale : Float -> Production -> Production
scale factor p =
    { quarks = p.quarks * factor
    , neutrons = p.neutrons * factor
    , protons = p.protons * factor
    , electrons = p.electrons * factor
    , hydrogen = p.hydrogen * factor
    , helium = p.helium * factor
    }


combine : Production -> Production -> Production
combine a b =
    { quarks = a.quarks + b.quarks
    , neutrons = a.neutrons + b.neutrons
    , protons = a.protons + b.protons
    , electrons = a.electrons + b.electrons
    , hydrogen = a.hydrogen + b.hydrogen
    , helium = a.helium + b.helium
    }


sum : List Production -> Production
sum productions =
    List.foldl combine empty productions


has : Production -> Production -> Bool
has a b =
    -- This is probably not the most efficient way to do this, but it's easy to add to for now.
    List.all
        identity
        [ a.quarks >= b.quarks
        , a.neutrons >= b.neutrons
        , a.protons >= b.protons
        , a.electrons >= b.electrons
        , a.hydrogen >= b.hydrogen
        , a.helium >= b.helium
        ]


valid : Production -> Bool
valid production =
    has production empty
