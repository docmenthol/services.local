module Page.Clicker.Progress exposing (Progress, buy, empty, levelFor, sum)

import Page.Clicker.Building exposing (Building(..))


type alias Progress =
    { quarkCollector : Int
    , protonCollector : Int
    , neutronCollector : Int
    , electronCollector : Int
    , hydrogenCollector : Int
    , heliumCollector : Int
    }


empty : Progress
empty =
    { quarkCollector = 0
    , protonCollector = 0
    , neutronCollector = 0
    , electronCollector = 0
    , hydrogenCollector = 0
    , heliumCollector = 0
    }


levelFor : Building -> Progress -> Int
levelFor building progress =
    case building of
        QuarkCollector ->
            progress.quarkCollector

        NeutronCollector ->
            progress.neutronCollector

        ProtonCollector ->
            progress.protonCollector

        ElectronCollector ->
            progress.electronCollector

        HydrogenCollector ->
            progress.hydrogenCollector

        HeliumCollector ->
            progress.heliumCollector


combine : Progress -> Progress -> Progress
combine a b =
    { quarkCollector = a.quarkCollector + b.quarkCollector
    , protonCollector = a.protonCollector + b.protonCollector
    , neutronCollector = a.neutronCollector + b.neutronCollector
    , electronCollector = a.electronCollector + b.electronCollector
    , hydrogenCollector = a.hydrogenCollector + b.hydrogenCollector
    , heliumCollector = a.heliumCollector + b.heliumCollector
    }


sum : List Progress -> Progress
sum progress =
    List.foldl combine empty progress


buy : Progress -> Building -> Progress
buy progress building =
    case building of
        QuarkCollector ->
            { progress | quarkCollector = progress.quarkCollector + 1 }

        NeutronCollector ->
            { progress | neutronCollector = progress.neutronCollector + 1 }

        ProtonCollector ->
            { progress | protonCollector = progress.protonCollector + 1 }

        ElectronCollector ->
            { progress | electronCollector = progress.electronCollector + 1 }

        HydrogenCollector ->
            { progress | hydrogenCollector = progress.hydrogenCollector + 1 }

        HeliumCollector ->
            { progress | heliumCollector = progress.heliumCollector + 1 }
