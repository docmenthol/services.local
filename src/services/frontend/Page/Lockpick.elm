module Page.Lockpick exposing (Model, Msg(..), init, subscriptions, toSession, update, view)

import Api
import Api.Endpoint as Endpoint
import Api.Lock as Lock exposing (Lock)
import Api.Pick as Pick exposing (Pick)
import Api.User as User exposing (User)
import Errors
import Html exposing (Html, div, h2, label, option, select, text)
import Html.Attributes exposing (class, for, name, value)
import Html.Events exposing (onClick)
import Http
import Json.Decode as D
import LoadingState exposing (LoadingState(..))
import Page exposing (divider)
import Route
import Session exposing (Session(..))
import Svg exposing (path, svg)
import Svg.Attributes as SA


type alias Model =
    { locks : LoadingState Http.Error (List Lock)
    , picks : LoadingState Http.Error (List Pick)
    , users : LoadingState Http.Error (List User)
    , lockFilter : Maybe Int
    , pickFilter : Maybe Int
    , session : Session
    }


type Msg
    = GotLocks (Result Http.Error (List Lock))
    | GotPicks (Result Http.Error (List Pick))
    | GotUsers (Result Http.Error (List User))
    | LockFilter String
    | PickFilter String
    | GotSession Session


init : Session -> ( Model, Cmd Msg )
init session =
    let
        locks =
            Endpoint.locks []

        picks =
            Endpoint.picks []

        users =
            Endpoint.users []
    in
    ( { locks = Loading, picks = Loading, users = Loading, lockFilter = Nothing, pickFilter = Nothing, session = session }
    , case session of
        Authenticated _ _ _ ->
            Cmd.batch
                [ Api.get locks (Session.cred session) GotLocks <| D.list Lock.decoder
                , Api.get picks (Session.cred session) GotPicks <| D.list Pick.decoder
                , Api.get users (Session.cred session) GotUsers <| D.list User.decoder
                ]

        Guest _ ->
            Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession <| Session.navKey model.session


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotLocks result ->
            case result of
                Ok locks ->
                    ( { model | locks = Loaded locks }, Cmd.none )

                Err err ->
                    ( { model | locks = Failed err }, Cmd.none )

        GotPicks result ->
            case result of
                Ok picks ->
                    ( { model | picks = Loaded picks }, Cmd.none )

                Err err ->
                    ( { model | picks = Failed err }, Cmd.none )

        GotUsers result ->
            case result of
                Ok users ->
                    ( { model | users = Loaded users }, Cmd.none )

                Err err ->
                    ( { model | users = Failed err }, Cmd.none )

        LockFilter filter ->
            ( { model | lockFilter = String.toInt filter }, Cmd.none )

        PickFilter filter ->
            ( { model | pickFilter = String.toInt filter }, Cmd.none )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "lockpick"
    , content =
        div [ class "content-slim" ]
            [ h2 [ class "heading object-center py-4 block w-full text-center" ] [ text "lockpick" ]
            , divider
            , case model.session of
                Authenticated _ _ _ ->
                    viewLoggedIn model

                Guest _ ->
                    div [ class "text-center" ] [ text "You must be logged in to view this page." ]
            ]
    }


viewLoggedIn : Model -> Html Msg
viewLoggedIn model =
    div [ class "flex flex-wrap mb-4" ] <|
        List.concat
            [ [ viewLockSelect model
              , viewPickSelect model
              ]
            , case model.picks of
                Loaded picks ->
                    [ viewPicksForLock picks model.lockFilter
                    , viewLocksForPick picks model.pickFilter
                    ]

                _ ->
                    []
            ]


viewLockOption : Lock -> Html Msg
viewLockOption lock =
    let
        id =
            String.fromInt lock.id
    in
    option [ value id, onClick <| LockFilter id ] [ text lock.name ]


viewPickOption : User -> Html Msg
viewPickOption user =
    let
        id =
            String.fromInt user.id
    in
    option [ value id, onClick <| PickFilter id ] [ text user.username ]


viewLockSelect : Model -> Html Msg
viewLockSelect model =
    let
        filterValue =
            case model.lockFilter of
                Just int ->
                    String.fromInt int

                Nothing ->
                    ""
    in
    div
        [ class "mb-6 px-2 relative w-1/2" ]
    <|
        List.concat
            [ [ label [ class "form__label", for "lock" ] [ text "select lock" ] ]
            , case model.locks of
                Loaded locks ->
                    [ select
                        [ class "form__select", name "lock", value filterValue ]
                      <|
                        List.map viewLockOption locks
                    , div
                        [ class "form__select-arrow pt-6 pr-6" ]
                        [ svg
                            [ SA.class "fill-current h-4 w-4", SA.viewBox "0 0 20 20" ]
                            [ path [ SA.d "M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z", SA.fill "white" ] [] ]
                        ]
                    ]

                Loading ->
                    [ text "Loading..." ]

                Failed err ->
                    [ text <| Errors.toString err ]

                NotLoading ->
                    []
            ]


viewPicksForLock : List Pick -> Maybe Int -> Html Msg
viewPicksForLock picks lockFilter =
    case lockFilter of
        Just lockID ->
            let
                names =
                    List.filterMap
                        (\p ->
                            if p.lockID == lockID then
                                Just <| div [ class "rounded shadow-md p-2 mx-2" ] [ text p.pickName ]

                            else
                                Nothing
                        )
                        picks
            in
            div [ class "w-1/2 px-2 flex flex-wrap" ] names

        Nothing ->
            div [ class "w-1/2 px-2" ] [ text "Select a lock." ]


viewPickSelect : Model -> Html Msg
viewPickSelect model =
    let
        filterValue =
            case model.pickFilter of
                Just int ->
                    String.fromInt int

                Nothing ->
                    ""
    in
    div
        [ class "mb-6 px-2 relative w-1/2" ]
    <|
        List.concat
            [ [ label [ class "form__label", for "pick" ] [ text "select pick" ] ]
            , case model.users of
                Loaded users ->
                    [ select
                        [ class "form__select", name "pick", value filterValue ]
                      <|
                        List.map viewPickOption users
                    , div
                        [ class "form__select-arrow pt-6 pr-6" ]
                        [ svg
                            [ SA.class "fill-current h-4 w-4", SA.viewBox "0 0 20 20" ]
                            [ path [ SA.d "M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z", SA.fill "white" ] [] ]
                        ]
                    ]

                Loading ->
                    [ text "Loading..." ]

                Failed err ->
                    [ text <| Errors.toString err ]

                NotLoading ->
                    []
            ]


viewLocksForPick : List Pick -> Maybe Int -> Html Msg
viewLocksForPick picks pickFilter =
    case pickFilter of
        Just pickID ->
            let
                names =
                    List.filterMap
                        (\p ->
                            if p.pickID == pickID then
                                Just <| div [ class "rounded shadow-md p-2 mx-2" ] [ text p.lockName ]

                            else
                                Nothing
                        )
                        picks
            in
            div [ class "w-1/2 px-2 flex flex-wrap" ] names

        Nothing ->
            div [ class "w-1/2 px-2" ] [ text "Select a pick." ]


toSession : Model -> Session
toSession model =
    model.session
