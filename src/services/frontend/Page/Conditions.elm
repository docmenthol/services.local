module Page.Conditions exposing (Model, Msg(..), init, subscriptions, toSession, update, view)

import Api
import Api.Conditions as Conditions exposing (Condition)
import Api.Endpoint exposing (conditions, recentConditions)
import Autotable as AT
import Autotable.Options as ATO
import Errors
import Html exposing (Html, div, h2, h3, text)
import Html.Attributes exposing (class)
import Http
import Json.Decode as D
import LoadingState exposing (LoadingState(..))
import Page exposing (divider)
import Route
import Session exposing (Session)
import Time exposing (Month(..))


type alias Model =
    { recent : LoadingState Http.Error (AT.Model Msg Condition)
    , current : LoadingState Http.Error Condition
    , zone : Time.Zone
    , session : Session
    }


type Msg
    = GotCurrentCondition (Result Http.Error Condition)
    | GotRecentConditions (Result Http.Error (List Condition))
    | GotSession Session
    | TableMsg AT.Msg


intSort : (Condition -> Int) -> Condition -> String
intSort fn d =
    String.fromInt <| fn d


floatSort : (Condition -> Float) -> Condition -> String
floatSort fn d =
    String.fromFloat <| fn d


intFilter : (Condition -> Int) -> Condition -> String -> Bool
intFilter fn d s =
    String.startsWith s <| String.fromInt <| fn d


floatFilter : (Condition -> Float) -> Condition -> String -> Bool
floatFilter fn d s =
    String.startsWith s <| String.fromFloat <| fn d


conditionColumns : List (AT.Column Msg Condition)
conditionColumns =
    [ AT.Column
        "Datetime"
        "datetime"
        (\c -> viewDatetime Time.utc c.dt)
        (\c _ -> text <| String.fromInt c.dt)
        (intSort .dt)
        (intFilter .dt)
        (\c _ -> c)
    , AT.Column
        "Temp"
        "temp"
        (\c -> String.fromFloat c.temperature ++ "°C")
        (\c _ -> text <| String.fromFloat c.temperature)
        (floatSort .temperature)
        (floatFilter .temperature)
        (\c _ -> c)
    , AT.Column
        "Brightness"
        "brightness"
        (\c -> String.fromInt c.lumens ++ "lm")
        (\c _ -> text <| String.fromInt c.lumens)
        (intSort .lumens)
        (intFilter .lumens)
        (\c _ -> c)
    ]


tableOptions : ATO.Options
tableOptions =
    ATO.Options ATO.NoSorting ATO.NoFiltering ATO.NoSelecting ATO.NoDragging ATO.NoEditing ATO.NoPagination ATO.NoFill


init : Session -> ( Model, Cmd Msg )
init session =
    let
        cred =
            Session.cred session
    in
    ( { recent = Loading, current = Loading, zone = Time.utc, session = session }
    , Cmd.batch
        [ Api.get conditions cred GotCurrentCondition Conditions.decoder
        , Api.get recentConditions Nothing GotRecentConditions <| D.list Conditions.decoder
        ]
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model.session)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotCurrentCondition result ->
            case result of
                Ok condition ->
                    ( { model | current = Loaded condition }, Cmd.none )

                Err err ->
                    ( { model | current = Failed err }, Cmd.none )

        GotRecentConditions result ->
            case result of
                Ok conditions ->
                    let
                        tableState =
                            AT.init "conditions" conditionColumns conditions tableOptions
                    in
                    ( { model | recent = Loaded tableState }, Cmd.none )

                Err err ->
                    ( { model | recent = Failed err }, Cmd.none )

        TableMsg tableMsg ->
            let
                recent =
                    LoadingState.map model.recent <| AT.update tableMsg
            in
            ( { model | recent = recent }, Cmd.none )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


monthNum : Time.Month -> String
monthNum month =
    case month of
        Jan ->
            "1"

        Feb ->
            "2"

        Mar ->
            "3"

        Apr ->
            "4"

        May ->
            "5"

        Jun ->
            "6"

        Jul ->
            "7"

        Aug ->
            "8"

        Sep ->
            "9"

        Oct ->
            "10"

        Nov ->
            "11"

        Dec ->
            "12"


viewDatetime : Time.Zone -> Int -> String
viewDatetime zone dt =
    let
        t =
            Time.millisToPosix dt
    in
    (String.fromInt <| Time.toDay zone t)
        ++ "/"
        ++ (monthNum <| Time.toMonth zone t)
        ++ ", "
        ++ (String.padLeft 2 '0' <| String.fromInt <| Time.toHour zone t)
        ++ ":"
        ++ (String.padLeft 2 '0' <| String.fromInt <| Time.toMinute zone t)
        ++ ":"
        ++ (String.padLeft 2 '0' <| String.fromInt <| Time.toSecond zone t)


viewCurrentConditions : Condition -> Html msg
viewCurrentConditions condition =
    div
        [ class "flex flex-wrap justify-center" ]
        [ div
            [ class "w-full text-center mt-5" ]
            [ text "Sampled At ", text <| viewDatetime Time.utc condition.dt ]
        , div
            [ class "ml-auto rounded shadow-md border-gray-900 text-center p-5 mx-2 text-4xl" ]
            [ text <| String.fromFloat condition.temperature, text "°C" ]
        , div
            [ class "mr-auto rounded shadow-md border-gray-900 text-center p-5 mx-2 text-4xl" ]
            [ text <| String.fromInt condition.lumens, text "lm" ]
        ]


view : Model -> { title : String, content : Html Msg }
view model =
    let
        currentConditions =
            case model.current of
                NotLoading ->
                    div [ class "w-full text-center" ] [ text "Idle..." ]

                Loading ->
                    div [ class "w-full text-center" ] [ text "Loading..." ]

                Loaded condition ->
                    viewCurrentConditions condition

                Failed err ->
                    div [ class "w-full text-center" ] [ text <| Errors.toString err ]

        recentConditions =
            case model.recent of
                NotLoading ->
                    div [ class "w-full text-center" ] [ text "Idle..." ]

                Loading ->
                    div [ class "w-full text-center" ] [ text "Loading..." ]

                Loaded conditions ->
                    AT.view conditions TableMsg

                Failed err ->
                    div [ class "w-full text-center" ] [ text <| Errors.toString err ]
    in
    { title = "conditions"
    , content =
        div [ class "content-slim" ]
            [ h2 [ class "heading object-center py-4 block w-full text-center" ] [ text "conditions" ]
            , divider
            , div [ class "flex" ]
                [ div
                    [ class "w-1/2" ]
                    [ div [ class "w-full text-center" ] [ h3 [] [ text "current conditions" ] ]
                    , currentConditions
                    ]
                , div
                    [ class "w-1/2" ]
                    [ div
                        []
                        [ div [ class "w-full text-center" ] [ h3 [] [ text "recent conditions" ] ]
                        , recentConditions
                        ]
                    ]
                ]
            ]
    }


toSession : Model -> Session
toSession model =
    model.session
