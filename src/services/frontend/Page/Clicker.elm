module Page.Clicker exposing (Model, Msg(..), init, subscriptions, toSession, update, view)

import Browser.Events exposing (onAnimationFrameDelta)
import Html exposing (Html, button, div, hr, span, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Page.Clicker.Building as Building exposing (Building(..))
import Page.Clicker.Particle as Particle exposing (Particle(..))
import Page.Clicker.Production as Production exposing (Production)
import Page.Clicker.Progress as Progress exposing (Progress)
import Route
import Session exposing (Session)
import Time


type alias Model =
    { progress : Progress
    , production : Production
    , session : Session
    }


type Msg
    = GotSession Session
    | BuildingTick Float
    | LocalSaveTick Time.Posix
    | RemoteSaveTick Time.Posix
    | CreateParticle Particle
    | CreateBuilding Building


init : Session -> ( Model, Cmd Msg )
init session =
    ( { progress = Progress.empty
      , production = Production.empty
      , session = session
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch <|
        [ Session.changes GotSession <| Session.navKey model.session
        , onAnimationFrameDelta ((\dt -> dt / 1000) >> BuildingTick)
        , Time.every 1000 LocalSaveTick
        , Time.every 5000 RemoteSaveTick
        ]


calculateProduction : Model -> Building -> Production
calculateProduction model building =
    let
        level =
            toFloat <| Progress.levelFor building model.progress

        production =
            Production.scale level <|
                Building.productionFor building
    in
    if level > 0 && Production.valid production then
        production

    else
        Production.empty


productionForTick : Model -> Float -> Production
productionForTick model scale =
    Production.scale scale <|
        List.foldl Production.combine Production.empty <|
            List.map (calculateProduction model) Building.all


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )

        -- I'm certain there's a better way to write basically all of this.
        BuildingTick dt ->
            let
                production =
                    productionForTick model dt
            in
            ( { model | production = Production.combine model.production production }
            , Cmd.none
            )

        LocalSaveTick _ ->
            ( model, Cmd.none )

        RemoteSaveTick _ ->
            ( model, Cmd.none )

        -- Production Click Signals
        CreateParticle particle ->
            let
                cost =
                    Production.scale -1.0 <| Particle.cost particle

                production =
                    Production.sum [ model.production, cost, Particle.produce particle ]
            in
            ( { model | production = production }, Cmd.none )

        CreateBuilding building ->
            let
                cost =
                    Production.scale -1.0 <|
                        Building.calculateCost building <|
                            Progress.levelFor building model.progress
                                + 1

                production =
                    Production.combine model.production cost

                progress =
                    Progress.buy model.progress building
            in
            ( { model | progress = progress, production = production }, Cmd.none )


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "clicker"
    , content =
        div
            [ class "content-slim flex" ]
            [ div [ class "w-2/3" ] [ viewControls model ]
            , div [ class "w-1/3" ] [ viewResources model ]
            ]
    }


viewParticleControlButtons : Model -> Building -> Particle -> Html Msg
viewParticleControlButtons model building particle =
    let
        nextLevel =
            Progress.levelFor building model.progress + 1

        particleCost =
            Particle.cost particle

        particleClasses =
            if Production.has model.production particleCost then
                "button"

            else
                "button button-disabled"

        collectorCost =
            Building.calculateCost building nextLevel

        collectorClasses =
            if Production.has model.production collectorCost then
                "button"

            else
                "button button-disabled"
    in
    div [ class "w-full flex p-4" ]
        [ div [ class "w-9/12" ] [ text <| Particle.toString particle ]
        , div [ class "w-1/12" ] [ button [ class particleClasses, onClick <| CreateParticle particle ] [ text "+", Particle.toHtml particle ] ]
        , div [ class "w-2/12 px-2" ] [ button [ class collectorClasses, onClick <| CreateBuilding building ] [ text "+ collector" ] ]
        ]


viewControls : Model -> Html Msg
viewControls model =
    div
        [ class "flex flex-wrap" ]
        [ viewParticleControlButtons model QuarkCollector Quark
        , viewParticleControlButtons model NeutronCollector Neutron
        , viewParticleControlButtons model ProtonCollector Proton
        , viewParticleControlButtons model ElectronCollector Electron
        , viewParticleControlButtons model HydrogenCollector Hydrogen
        , viewParticleControlButtons model HeliumCollector Helium
        ]


viewResourceText : Float -> String
viewResourceText resource =
    String.fromInt <| round resource


viewResources : Model -> Html Msg
viewResources model =
    let
        qps =
            .quarks <|
                productionForTick model 1.0
    in
    div
        [ class "flex flex-wrap" ]
        [ div
            [ class "w-full text-center" ]
            [ span [ class "text-6xl block" ] [ text <| viewResourceText model.production.quarks ]
            , span [ class "text-2xl" ] [ text "quarks", text <| " (" ++ String.fromFloat qps ++ "/s)" ]
            ]
        , div [ class "w-full py-4" ] [ hr [ class "border-gray-300" ] [] ]
        , div
            [ class "w-full flex flex-wrap leading-loose" ]
            [ div [ class "w-1/3" ] [ text "neutrons" ]
            , div [ class "w-2/3 text-right" ] [ text <| viewResourceText model.production.neutrons ]
            , div [ class "w-1/3" ] [ text "protons" ]
            , div [ class "w-2/3 text-right" ] [ text <| viewResourceText model.production.protons ]
            , div [ class "w-1/3" ] [ text "electrons" ]
            , div [ class "w-2/3 text-right" ] [ text <| viewResourceText model.production.electrons ]
            ]
        ]


toSession : Model -> Session
toSession model =
    model.session
