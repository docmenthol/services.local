module Page.Admin.Games exposing
    ( Model
    , Msg(..)
    , init
    , subscriptions
    , toSession
    , update
    , view
    )

import Api
import Api.Endpoint as Endpoint
import Api.Game as Game exposing (Game)
import Array
import Autotable as AT
import Autotable.Options as ATO
import Html exposing (Html, div, input, text)
import Html.Attributes exposing (checked, class, type_, value)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as D
import LoadingState exposing (LoadingState(..))
import Route
import Session exposing (Session)


type alias Model =
    { games : LoadingState Http.Error (AT.Model Msg Game)
    , session : Session
    }


type Msg
    = GotGames (Result Http.Error (List Game))
    | TableMsg AT.Msg
    | Edit String Int String
    | UpdatedGame Int Game (Result Http.Error ())
    | GotSession Session


intSort : (Game -> Int) -> Game -> String
intSort fn d =
    String.fromInt <| fn d


stringFilter : (Game -> String) -> Game -> String -> Bool
stringFilter fn d s =
    let
        ls =
            String.toLower s
    in
    String.startsWith ls <| String.toLower <| fn d


intFilter : (Game -> Int) -> Game -> String -> Bool
intFilter fn d s =
    String.startsWith s <| String.fromInt <| fn d


viewTextInput : String -> Game -> Int -> Html Msg
viewTextInput key game index =
    input [ type_ "text", class "w-full", value game.name, onInput <| Edit key index ] []


viewCheckInput : String -> Game -> Int -> Html Msg
viewCheckInput key game index =
    let
        prefix =
            case game.prefix of
                0 ->
                    "1"

                _ ->
                    "0"
    in
    input
        [ type_ "checkbox"
        , class "form-checkbox h-6 w-6 ml-2"
        , checked <| game.prefix /= 0
        , onClick <| Edit key index prefix
        ]
        []


columns : List (AT.Column Msg Game)
columns =
    [ AT.Column
        "Name"
        "name"
        .name
        (viewTextInput "name")
        .name
        (stringFilter .name)
        (\r v -> { r | name = v })
    , AT.Column
        "Units"
        "units"
        .units
        (viewTextInput "units")
        .units
        (stringFilter .units)
        (\r v -> { r | units = v })
    , AT.Column
        "Prefix"
        "prefix"
        (\r ->
            if r.prefix == 0 then
                "No"

            else
                "Yes"
        )
        (viewCheckInput "prefix")
        (intSort .prefix)
        (intFilter .prefix)
        (\r v ->
            let
                prefix =
                    case String.toInt v of
                        Just p ->
                            p

                        Nothing ->
                            0
            in
            { r | prefix = prefix }
        )
    ]


tableOptions : ATO.Options
tableOptions =
    ATO.Options ATO.NoSorting ATO.NoFiltering ATO.NoSelecting ATO.NoDragging ATO.Editing ATO.NoPagination (ATO.Fill 10)


init : Session -> ( Model, Cmd Msg )
init session =
    let
        endpoint =
            Endpoint.games []
    in
    ( { games = Loading, session = session }
    , Api.get endpoint Nothing GotGames <| D.list Game.decoder
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model.session)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotGames result ->
            case result of
                Ok games ->
                    ( { model | games = Loaded <| AT.init "admin-games" columns games tableOptions }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        TableMsg (AT.FinishEdit index) ->
            let
                oldGame =
                    case model.games of
                        Loaded newGames ->
                            Array.get index newGames.data

                        _ ->
                            Nothing

                games =
                    LoadingState.map model.games <| AT.update (AT.FinishEdit index)

                newGame =
                    case model.games of
                        Loaded newGames ->
                            Array.get index newGames.data

                        _ ->
                            Nothing

                cmd =
                    case ( oldGame, newGame ) of
                        ( Just og, Just ng ) ->
                            if not <| Game.equal og ng then
                                Api.postNoResp
                                    (Endpoint.games [ "update", String.fromInt <| og.id ])
                                    (Session.cred model.session)
                                    (Http.jsonBody <| Game.encode ng)
                                    (UpdatedGame index og)

                            else
                                Cmd.none

                        _ ->
                            Cmd.none
            in
            ( { model | games = games }, cmd )

        TableMsg tableMsg ->
            let
                games =
                    LoadingState.map model.games <| AT.update tableMsg
            in
            ( { model | games = games }, Cmd.none )

        Edit key index value ->
            let
                tableMsg =
                    AT.Edit key index value

                games =
                    LoadingState.map model.games <| AT.update tableMsg
            in
            ( { model | games = games }, Cmd.none )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )

        UpdatedGame index oldGame result ->
            case result of
                Ok _ ->
                    ( model, Cmd.none )

                Err _ ->
                    let
                        revertedGames =
                            case model.games of
                                Loaded games ->
                                    Loaded <| { games | data = Array.set index oldGame games.data }

                                _ ->
                                    model.games
                    in
                    ( { model | games = revertedGames }, Cmd.none )


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "admin / games"
    , content =
        div
            [ class "content-slim" ]
        <|
            case model.games of
                NotLoading ->
                    [ text "Idle..." ]

                Loading ->
                    [ text "Loading..." ]

                Loaded games ->
                    [ AT.view games TableMsg ]

                Failed _ ->
                    [ text "Failed to load games." ]
    }


toSession : Model -> Session
toSession model =
    model.session
