module Page.Admin.Players exposing
    ( Model
    , Msg(..)
    , init
    , subscriptions
    , toSession
    , update
    , view
    )

import Api
import Api.Endpoint as Endpoint
import Api.Player as Player exposing (Player)
import Autotable as AT
import Autotable.Options as ATO
import Html exposing (Html, div, input, text)
import Html.Attributes exposing (class, type_, value)
import Http
import Json.Decode as D
import LoadingState exposing (LoadingState(..))
import Route
import Session exposing (Session)


type alias Model =
    { players : LoadingState Http.Error (AT.Model Msg Player)
    , session : Session
    }


type Msg
    = GotPlayers (Result Http.Error (List Player))
    | TableMsg AT.Msg
    | GotSession Session



--stringSort : (Player -> String) -> Player -> String
--stringSort fn d =
--    fn d
--intSort : (Player -> Int) -> Player -> String
--intSort fn d =
--    String.fromInt <| fn d


stringFilter : (Player -> String) -> Player -> String -> Bool
stringFilter fn d s =
    let
        ls =
            String.toLower s
    in
    String.startsWith ls <| String.toLower <| fn d


intFilter : (Player -> Int) -> Player -> String -> Bool
intFilter fn d s =
    String.startsWith s <| String.fromInt <| fn d


floatFilter : (Player -> Float) -> Player -> String -> Bool
floatFilter fn d s =
    String.startsWith s <| String.fromFloat <| fn d


columns : List (AT.Column Msg Player)
columns =
    [ AT.Column
        "User"
        "user"
        .username
        (\r _ -> input [ value <| String.fromInt r.userID ] [])
        .userID
        (intFilter .userID)
        (\r v ->
            case String.toInt v of
                Just id ->
                    { r | userID = id }

                Nothing ->
                    r
        )
    , AT.Column
        "Name"
        "name"
        .name
        (\r _ -> input [ type_ "text", class "w-full", value r.name ] [])
        .name
        (stringFilter .name)
        (\r v -> { r | name = v })
    , AT.Column
        "Pupillary Distance"
        "pd"
        .pd
        (\r _ -> input [ type_ "text", class "w-full", value r.pd ] [])
        .pd
        (floatFilter .pd)
        (\r v -> { r | pd = v })
    ]


tableOptions : ATO.Options
tableOptions =
    ATO.Options ATO.NoSorting ATO.NoFiltering ATO.NoSelecting ATO.NoDragging ATO.Editing ATO.NoPagination (ATO.Fill 10)


init : Session -> ( Model, Cmd Msg )
init session =
    let
        endpoint =
            Endpoint.players []
    in
    ( { players = Loading, session = session }
    , Api.get endpoint Nothing GotPlayers <| D.list Player.decoder
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model.session)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotPlayers result ->
            case result of
                Ok players ->
                    ( { model | players = Loaded <| AT.init "admin-players" columns players tableOptions }
                    , Cmd.none
                    )

                Err err ->
                    ( { model | players = Failed err }, Cmd.none )

        TableMsg tableMsg ->
            let
                players =
                    LoadingState.map model.players <| AT.update tableMsg
            in
            ( { model | players = players }, Cmd.none )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "admin / players"
    , content =
        div
            [ class "content-slim" ]
        <|
            case model.players of
                NotLoading ->
                    [ text "Idle..." ]

                Loading ->
                    [ text "Loading..." ]

                Loaded players ->
                    [ AT.view players TableMsg ]

                Failed _ ->
                    [ text "Failed to load players." ]
    }


toSession : Model -> Session
toSession model =
    model.session
