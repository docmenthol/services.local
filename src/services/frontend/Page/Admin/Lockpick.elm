module Page.Admin.Lockpick exposing
    ( Model
    , Msg(..)
    , init
    , subscriptions
    , toSession
    , update
    , view
    )

import Api
import Api.Endpoint as Endpoint
import Api.Lock as Lock exposing (Lock)
import Api.Pick as Pick exposing (Pick)
import Api.User as User exposing (User)
import Elements.Swapper as Swapper
import Html exposing (Html, button, div, label, option, select, text)
import Html.Attributes exposing (class, for, value)
import Html.Events exposing (onClick)
import Http
import Json.Decode as D
import Json.Encode as E
import LoadingState exposing (LoadingState(..))
import Route
import Session exposing (Session)
import Svg exposing (path, svg)
import Svg.Attributes as SA


type alias Model =
    { users : LoadingState Http.Error (List User)
    , locks : LoadingState Http.Error (List Lock)
    , picks : LoadingState Http.Error (List Pick)
    , userFilter : Maybe String
    , swapperState : Maybe Swapper.Model
    , session : Session
    }


type Msg
    = GotUsers (Result Http.Error (List User))
    | GotLocks (Result Http.Error (List Lock))
    | GotPicks (Result Http.Error (List Pick))
    | UserFilter String
    | SwapperMsg Swapper.Msg
    | Save
    | Saved (Result Http.Error ())
    | GotSession Session


encodeBody : Model -> E.Value
encodeBody model =
    E.object
        [ ( "user_id"
          , case model.userFilter of
                Just userID ->
                    E.string userID

                Nothing ->
                    E.null
          )
        , ( "locks"
          , case model.swapperState of
                Just swapper ->
                    E.list E.string swapper.enabled

                Nothing ->
                    E.list E.string []
          )
        ]


init : Session -> ( Model, Cmd Msg )
init session =
    let
        cred =
            Session.cred session
    in
    ( { users = Loading
      , locks = Loading
      , picks = Loading
      , swapperState = Nothing
      , userFilter = Nothing
      , session = session
      }
    , Cmd.batch
        [ Api.get (Endpoint.users []) cred GotUsers <| D.list User.decoder
        , Api.get (Endpoint.locks []) cred GotLocks <| D.list Lock.decoder
        ]
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model.session)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotUsers result ->
            case result of
                Ok users ->
                    ( { model | users = Loaded users }, Cmd.none )

                Err err ->
                    ( { model | users = Failed err }, Cmd.none )

        GotLocks result ->
            case result of
                Ok locks ->
                    ( { model | locks = Loaded <| Lock.sort locks }, Cmd.none )

                Err err ->
                    ( { model | locks = Failed err }, Cmd.none )

        GotPicks result ->
            case result of
                Ok picks ->
                    case model.locks of
                        Loaded locks ->
                            let
                                ps =
                                    List.map (\p -> String.fromInt p.lockID) picks

                                swapper =
                                    Swapper.init locks .name (\_ l -> String.fromInt l.id)

                                ( enabled, available ) =
                                    List.partition
                                        (\l -> List.member (String.fromInt l.id) ps)
                                        locks

                                swapperState =
                                    { swapper
                                        | available = List.map (\l -> String.fromInt l.id) available
                                        , enabled = List.map (\l -> String.fromInt l.id) enabled
                                    }
                            in
                            ( { model | picks = Loaded picks, swapperState = Just swapperState }, Cmd.none )

                        _ ->
                            ( { model | picks = Loaded picks }, Cmd.none )

                Err err ->
                    ( { model | picks = Failed err }, Cmd.none )

        SwapperMsg Swapper.Enable ->
            case ( model.swapperState, model.locks ) of
                ( Just swapper, Loaded _ ) ->
                    let
                        --selectedLocks =
                        --    List.filter (\l -> List.member (String.fromInt l.id) swapper.availableSelected) locks
                        swapperState =
                            Just <| Swapper.update Swapper.Enable swapper
                    in
                    ( { model | swapperState = swapperState }, Cmd.none )

                ( _, _ ) ->
                    ( model, Cmd.none )

        SwapperMsg subMsg ->
            let
                swapperState =
                    case model.swapperState of
                        Just swapper ->
                            Just <| Swapper.update subMsg swapper

                        Nothing ->
                            Nothing
            in
            ( { model | swapperState = swapperState }
            , Cmd.none
            )

        Save ->
            let
                cred =
                    Session.cred model.session

                endpoint =
                    Endpoint.lockpick [ "update", "picks" ]

                body =
                    Http.jsonBody <| encodeBody model
            in
            ( model
            , Api.postNoResp endpoint cred body Saved
            )

        Saved _ ->
            ( model, Cmd.none )

        UserFilter filter ->
            ( { model | userFilter = Just filter }
            , Api.get
                (Endpoint.locks [ "pick", filter ])
                (Session.cred model.session)
                GotPicks
                (D.list Pick.decoder)
            )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "admin / lockpick"
    , content =
        div
            [ class "w-1/3 content" ]
        <|
            List.concat
                [ [ viewUserSelect model ]
                , case model.swapperState of
                    Just swapper ->
                        [ div [ class "w-full mt-2" ] [ Swapper.view swapper SwapperMsg ]
                        , div
                            [ class "flex justify-end pt-2" ]
                            [ div [ class "mt-2" ] [ button [ class "button", onClick Save ] [ text "Save" ] ] ]
                        ]

                    Nothing ->
                        []
                ]
    }


viewUserSelect : Model -> Html Msg
viewUserSelect model =
    let
        filterValue =
            Maybe.withDefault "" model.userFilter

        users =
            case model.users of
                Loaded us ->
                    us

                _ ->
                    []
    in
    div
        [ class "mb-6 px-2 relative w-full" ]
        [ label [ class "form__label", for "user" ] [ text "select user" ]
        , select
            [ class "form__select", value filterValue ]
          <|
            List.map
                (\u ->
                    let
                        id =
                            String.fromInt u.id
                    in
                    option [ value id, onClick <| UserFilter id ] [ text u.username ]
                )
                users
        , div
            [ class "form__select-arrow pt-6 pr-6" ]
            [ svg
                [ SA.class "fill-current h-4 w-4", SA.viewBox "0 0 20 20" ]
                [ path [ SA.d "M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z", SA.fill "white" ] [] ]
            ]
        ]


toSession : Model -> Session
toSession model =
    model.session
