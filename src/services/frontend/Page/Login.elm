module Page.Login exposing (Model, Msg(..), init, subscriptions, toSession, update, view)

import Api
import Api.Endpoint exposing (login)
import Errors
import Html exposing (Html, button, div, form, input, label, text)
import Html.Attributes exposing (class, for, name, type_)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Encode as E
import LoadingState exposing (LoadingState(..))
import Route
import Session exposing (Session)
import Viewer exposing (Viewer)


type alias Model =
    { username : String
    , password : String
    , loggingIn : LoadingState Http.Error ()
    , session : Session
    }


type Msg
    = ChangeUser String
    | ChangePassword String
    | SendLogin
    | LoginResult (Result Http.Error Viewer)
    | GotSession Session


encodeLogin : Model -> E.Value
encodeLogin v =
    E.object
        [ ( "username", E.string v.username )
        , ( "password", E.string v.password )
        ]


init : Session -> ( Model, Cmd Msg )
init session =
    ( { username = "", password = "", loggingIn = NotLoading, session = session }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model.session)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeUser username ->
            ( { model | username = username }, Cmd.none )

        ChangePassword password ->
            ( { model | password = password }, Cmd.none )

        SendLogin ->
            let
                body =
                    Http.jsonBody <| encodeLogin model
            in
            ( { model | loggingIn = Loading }
            , Api.login body LoginResult Viewer.decoder
            )

        LoginResult result ->
            case result of
                Ok viewer ->
                    ( { model | loggingIn = Loaded () }, Viewer.store viewer )

                Err err ->
                    ( { model | loggingIn = Failed err }, Cmd.none )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "login"
    , content =
        div [ class "content-tiny" ]
            [ div
                [ class "w-full max-w-ws" ]
                [ form [ class "form-mini" ]
                    [ viewField model "username" "text" ChangeUser
                    , viewField model "password" "password" ChangePassword
                    , div [ class "flex items-center justify-between" ]
                        [ button
                            [ class "button-light rounded", type_ "button", onClick SendLogin ]
                            [ text "Login" ]
                        , case model.loggingIn of
                            Failed err ->
                                div [] [ text <| Errors.toString err ]

                            Loading ->
                                div [] [ text "Logging in..." ]

                            Loaded _ ->
                                div [] [ text "Success. Welcome. " ]

                            _ ->
                                div [] []
                        ]
                    ]
                ]
            ]
    }


viewField : Model -> String -> String -> (String -> Msg) -> Html Msg
viewField model inputName inputType changeEvent =
    div [ class "mb-4" ]
        [ label [ class "form__label", for inputName ] [ text inputName ]
        , input [ class "form__input-text", name inputName, type_ inputType, onInput changeEvent ] [ text model.username ]
        ]


toSession : Model -> Session
toSession model =
    model.session
