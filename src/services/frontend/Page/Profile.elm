module Page.Profile exposing (Model, Msg(..), init, subscriptions, toSession, update, view)

import Api.Profile exposing (Profile)
import Html exposing (Html, div)
import Html.Attributes exposing (class)
import Http
import LoadingState exposing (LoadingState(..))
import Route
import Session exposing (Session)


type alias Model =
    { profile : LoadingState Http.Error Profile
    , session : Session
    }


type Msg
    = GotProfile (Result Http.Error Profile)
    | GotSession Session


init : Session -> ( Model, Cmd Msg )
init session =
    ( { profile = Loading, session = session }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession <| Session.navKey model.session


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotProfile result ->
            case result of
                Ok profile ->
                    ( { model | profile = Loaded profile }, Cmd.none )

                Err err ->
                    ( { model | profile = Failed err }, Cmd.none )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


view : Model -> { title : String, content : Html Msg }
view _ =
    { title = "profile"
    , content =
        div
            [ class "content-tiny" ]
            []
    }


toSession : Model -> Session
toSession model =
    model.session
