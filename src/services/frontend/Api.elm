port module Api exposing (Cred(..), application, decode, delete, get, level, login, logout, post, postNoResp, put, storeCredWith, username, viewerChanges)

import Api.Endpoint as Endpoint exposing (Endpoint)
import Browser
import Browser.Navigation as Nav
import Http exposing (Body)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode exposing (Value)
import Token exposing (Token)
import Url exposing (Url)
import Username exposing (Username)


type Cred
    = Cred Username Token Int


username : Cred -> Username
username (Cred u _ _) =
    u


level : Cred -> Int
level (Cred _ _ l) =
    l


credAuthHeader : Cred -> Http.Header
credAuthHeader (Cred _ t _) =
    Http.header "Authorization" ("Token " ++ Token.toString t)


credXTokenHeader : Cred -> Http.Header
credXTokenHeader (Cred _ t _) =
    Http.header "X-Token" <| Token.toString t


credDecoder : Decoder Cred
credDecoder =
    Decode.succeed Cred
        |> required "username" Username.decoder
        |> required "token" Token.decoder
        |> required "level" Decode.int


decode : Decoder (Cred -> viewer) -> Value -> Result Decode.Error viewer
decode decoder value =
    Decode.decodeValue Decode.string value
        |> Result.andThen (\s -> Decode.decodeString (Decode.field "user" <| decoderFromCred decoder) s)


port onStoreChange : (Value -> msg) -> Sub msg


viewerChanges : (Maybe viewer -> msg) -> Decoder (Cred -> viewer) -> Sub msg
viewerChanges toMsg decoder =
    onStoreChange (\v -> toMsg <| decodeFromChange decoder v)


decodeFromChange : Decoder (Cred -> viewer) -> Value -> Maybe viewer
decodeFromChange viewerDecoder val =
    Decode.decodeValue (storageDecoder viewerDecoder) val
        |> Result.toMaybe


storeCredWith : Cred -> Cmd msg
storeCredWith (Cred uname token lvl) =
    let
        json =
            Encode.object
                [ ( "user"
                  , Encode.object
                        [ ( "username", Username.encode uname )
                        , ( "token", Token.encode token )
                        , ( "level", Encode.int lvl )
                        ]
                  )
                ]
    in
    storeCache (Just json)



-- HTTP


get : Endpoint -> Maybe Cred -> (Result Http.Error a -> msg) -> Decoder a -> Cmd msg
get url maybeCred msg decoder =
    Endpoint.request
        { method = "GET"
        , url = url
        , expect = Http.expectJson msg decoder
        , headers =
            case maybeCred of
                Just cred ->
                    [ credAuthHeader cred, credXTokenHeader cred ]

                Nothing ->
                    []
        , body = Http.emptyBody
        , timeout = Nothing
        , withCredentials = False
        }


put : Endpoint -> Cred -> Body -> (Result Http.Error a -> msg) -> Decoder a -> Cmd msg
put url cred body msg decoder =
    Endpoint.request
        { method = "PUT"
        , url = url
        , expect = Http.expectJson msg decoder
        , headers = [ credAuthHeader cred, credXTokenHeader cred ]
        , body = body
        , timeout = Nothing
        , withCredentials = False
        }


post : Endpoint -> Maybe Cred -> Body -> (Result Http.Error a -> msg) -> Decoder a -> Cmd msg
post url maybeCred body msg decoder =
    Endpoint.request
        { method = "POST"
        , url = url
        , expect = Http.expectJson msg decoder
        , headers =
            case maybeCred of
                Just cred ->
                    [ credAuthHeader cred, credXTokenHeader cred ]

                Nothing ->
                    []
        , body = body
        , timeout = Nothing
        , withCredentials = False
        }


postNoResp : Endpoint -> Maybe Cred -> Body -> (Result Http.Error () -> msg) -> Cmd msg
postNoResp url maybeCred body msg =
    Endpoint.request
        { method = "POST"
        , url = url
        , expect = Http.expectWhatever msg
        , headers =
            case maybeCred of
                Just cred ->
                    [ credAuthHeader cred, credXTokenHeader cred ]

                Nothing ->
                    []
        , body = body
        , timeout = Nothing
        , withCredentials = False
        }


delete : Endpoint -> Cred -> Body -> (Result Http.Error a -> msg) -> Decoder a -> Cmd msg
delete url cred body msg decoder =
    Endpoint.request
        { method = "DELETE"
        , url = url
        , expect = Http.expectJson msg decoder
        , headers = [ credAuthHeader cred, credXTokenHeader cred ]
        , body = body
        , timeout = Nothing
        , withCredentials = False
        }


logout : Cmd msg
logout =
    storeCache Nothing


port storeCache : Maybe Value -> Cmd msg


application :
    Decoder (Cred -> viewer)
    ->
        { init : Maybe viewer -> Url -> Nav.Key -> ( model, Cmd msg )
        , onUrlChange : Url -> msg
        , onUrlRequest : Browser.UrlRequest -> msg
        , subscriptions : model -> Sub msg
        , update : msg -> model -> ( model, Cmd msg )
        , view : model -> Browser.Document msg
        }
    -> Program Value model msg
application viewerDecoder config =
    let
        init flags url navKey =
            let
                maybeViewer =
                    Decode.decodeValue Decode.string flags
                        |> Result.andThen (Decode.decodeString (storageDecoder viewerDecoder))
                        |> Result.toMaybe
            in
            config.init maybeViewer url navKey
    in
    Browser.application
        { init = init
        , onUrlChange = config.onUrlChange
        , onUrlRequest = config.onUrlRequest
        , subscriptions = config.subscriptions
        , update = config.update
        , view = config.view
        }


storageDecoder : Decoder (Cred -> viewer) -> Decoder viewer
storageDecoder viewerDecoder =
    Decode.field "user" (decoderFromCred viewerDecoder)


login : Body -> (Result Http.Error a -> msg) -> Decoder (Cred -> a) -> Cmd msg
login body msg decoder =
    post Endpoint.login Nothing body msg (decoderFromCred decoder)


decoderFromCred : Decoder (Cred -> a) -> Decoder a
decoderFromCred decoder =
    Decode.map2
        (\fromCred cred -> fromCred cred)
        decoder
        credDecoder



--cacheStorageKey : String
--cacheStorageKey =
--    "cache"
--credStorageKey : String
--credStorageKey =
--    "cred"
