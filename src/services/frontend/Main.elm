module Main exposing (main)

import Api
import Api.Endpoint as Endpoint
import Browser as Browser exposing (Document)
import Browser.Navigation as Nav
import Html
import Http
import Json.Encode exposing (Value)
import Page
import Page.Admin.Games as AdminGames
import Page.Admin.Lockpick as AdminLockpick
import Page.Admin.Players as AdminPlayers
import Page.Blank as Blank
import Page.Clicker as Clicker
import Page.Conditions as Conditions
import Page.Home as Home
import Page.Lockpick as Lockpick
import Page.Login as Login
import Page.NotFound as NotFound
import Page.Profile as Profile
import Page.Register as Register
import Page.Scores as Scores
import Route exposing (Route)
import Session exposing (Session)
import Url
import Viewer exposing (Viewer)


type Model
    = Redirect Session
    | NotFound Session
    | Home Home.Model
    | Login Login.Model
    | Register Register.Model
    | Profile Profile.Model
    | Clicker Clicker.Model
    | Conditions Conditions.Model
    | Scores Scores.Model
    | Lockpick Lockpick.Model
    | AdminGames AdminGames.Model
    | AdminPlayers AdminPlayers.Model
    | AdminLockpick AdminLockpick.Model


init : Maybe Viewer -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init maybeViewer url key =
    changeRouteTo (Route.fromUrl url)
        (Redirect (Session.fromViewer key maybeViewer))


view : Model -> Document Msg
view model =
    let
        viewer =
            Session.viewer <| toSession model

        viewPage page toMsg config =
            let
                { title, body } =
                    Page.view viewer page config
            in
            { title = title
            , body = List.map (Html.map toMsg) body
            }
    in
    case model of
        Redirect _ ->
            Page.view viewer Page.Other Blank.view

        NotFound _ ->
            Page.view viewer Page.Other NotFound.view

        Home home ->
            viewPage Page.Home HomeMsg <| Home.view home

        Login login ->
            viewPage Page.Login LoginMsg <| Login.view login

        Register register ->
            viewPage Page.Register RegisterMsg <| Register.view register

        Profile profile ->
            viewPage Page.Profile ProfileMsg <| Profile.view profile

        Clicker clicker ->
            viewPage Page.Clicker ClickerMsg <| Clicker.view clicker

        Conditions conditions ->
            viewPage Page.Conditions ConditionsMsg <| Conditions.view conditions

        Scores scores ->
            viewPage Page.Scores ScoresMsg <| Scores.view scores

        Lockpick lockpick ->
            viewPage Page.Lockpick LockpickMsg <| Lockpick.view lockpick

        AdminGames adminGames ->
            viewPage Page.AdminGames AdminGamesMsg <| AdminGames.view adminGames

        AdminPlayers adminPlayers ->
            viewPage Page.AdminPlayers AdminPlayersMsg <| AdminPlayers.view adminPlayers

        AdminLockpick adminLockpick ->
            viewPage Page.AdminLockpick AdminLockpickMsg <| AdminLockpick.view adminLockpick


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | NotFoundMsg NotFound.Msg
    | HomeMsg Home.Msg
    | LoginMsg Login.Msg
    | RegisterMsg Register.Msg
    | ProfileMsg Profile.Msg
    | ClickerMsg Clicker.Msg
    | ConditionsMsg Conditions.Msg
    | ScoresMsg Scores.Msg
    | LockpickMsg Lockpick.Msg
    | AdminGamesMsg AdminGames.Msg
    | AdminPlayersMsg AdminPlayers.Msg
    | AdminLockpickMsg AdminLockpick.Msg
    | GotSession Session
    | LoggedOut (Result Http.Error ())


toSession : Model -> Session
toSession model =
    case model of
        Redirect session ->
            session

        NotFound notFound ->
            notFound

        Home home ->
            Home.toSession home

        Login login ->
            Login.toSession login

        Register register ->
            Register.toSession register

        Profile profile ->
            Profile.toSession profile

        Clicker clicker ->
            Clicker.toSession clicker

        Conditions conditions ->
            Conditions.toSession conditions

        Scores scores ->
            Scores.toSession scores

        Lockpick lockpick ->
            Lockpick.toSession lockpick

        AdminGames adminGames ->
            AdminGames.toSession adminGames

        AdminPlayers adminPlayers ->
            AdminPlayers.toSession adminPlayers

        AdminLockpick adminLockpick ->
            AdminLockpick.toSession adminLockpick


changeRouteTo : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRouteTo maybeRoute model =
    let
        session =
            toSession model
    in
    case maybeRoute of
        Nothing ->
            ( NotFound session, Cmd.none )

        Just Route.Root ->
            ( model, Route.replaceUrl (Session.navKey session) Route.Home )

        Just Route.Home ->
            Home.init session
                |> updateWith Home HomeMsg model

        Just Route.Login ->
            Login.init session
                |> updateWith Login LoginMsg model

        Just Route.Register ->
            Register.init session
                |> updateWith Register RegisterMsg model

        Just Route.Profile ->
            Profile.init session
                |> updateWith Profile ProfileMsg model

        Just Route.Clicker ->
            Clicker.init session
                |> updateWith Clicker ClickerMsg model

        Just Route.Conditions ->
            Conditions.init session
                |> updateWith Conditions ConditionsMsg model

        Just Route.Scores ->
            Scores.init session
                |> updateWith Scores ScoresMsg model

        Just Route.Lockpick ->
            Lockpick.init session
                |> updateWith Lockpick LockpickMsg model

        Just Route.AdminGames ->
            AdminGames.init session
                |> updateWith AdminGames AdminGamesMsg model

        Just Route.AdminPlayers ->
            AdminPlayers.init session
                |> updateWith AdminPlayers AdminPlayersMsg model

        Just Route.AdminLockpick ->
            AdminLockpick.init session
                |> updateWith AdminLockpick AdminLockpickMsg model

        Just Route.Logout ->
            ( model
            , Cmd.batch
                [ Api.logout
                , Api.postNoResp Endpoint.logout (Session.cred session) Http.emptyBody LoggedOut
                ]
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        NotFound notFound ->
            Sub.map NotFoundMsg <| NotFound.subscriptions notFound

        Redirect _ ->
            Session.changes GotSession <| Session.navKey <| toSession model

        Home home ->
            Sub.map HomeMsg <| Home.subscriptions home

        Lockpick lockpick ->
            Sub.map LockpickMsg <| Lockpick.subscriptions lockpick

        Login login ->
            Sub.map LoginMsg <| Login.subscriptions login

        Register register ->
            Sub.map RegisterMsg <| Register.subscriptions register

        Profile profile ->
            Sub.map ProfileMsg <| Profile.subscriptions profile

        Clicker clicker ->
            Sub.map ClickerMsg <| Clicker.subscriptions clicker

        Conditions conditions ->
            Sub.map ConditionsMsg <| Conditions.subscriptions conditions

        Scores scores ->
            Sub.map ScoresMsg <| Scores.subscriptions scores

        AdminGames games ->
            Sub.map AdminGamesMsg <| AdminGames.subscriptions games

        AdminPlayers players ->
            Sub.map AdminPlayersMsg <| AdminPlayers.subscriptions players

        AdminLockpick lockpick ->
            Sub.map AdminLockpickMsg <| AdminLockpick.subscriptions lockpick


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( LinkClicked request, _ ) ->
            case request of
                Browser.Internal url ->
                    case url.fragment of
                        Nothing ->
                            ( model, Cmd.none )

                        Just _ ->
                            ( model
                            , Nav.pushUrl
                                (Session.navKey <| toSession model)
                                (Url.toString url)
                            )

                Browser.External href ->
                    ( model, Nav.load href )

        ( UrlChanged url, _ ) ->
            changeRouteTo (Route.fromUrl url) model

        ( NotFoundMsg subMsg, NotFound notFound ) ->
            NotFound.update subMsg notFound
                |> updateWith NotFound NotFoundMsg model

        ( NotFoundMsg _, _ ) ->
            ( model, Cmd.none )

        ( HomeMsg subMsg, Home home ) ->
            Home.update subMsg home
                |> updateWith Home HomeMsg model

        ( HomeMsg _, _ ) ->
            ( model, Cmd.none )

        ( LockpickMsg subMsg, Lockpick lockpick ) ->
            Lockpick.update subMsg lockpick
                |> updateWith Lockpick LockpickMsg model

        ( LockpickMsg _, _ ) ->
            ( model, Cmd.none )

        ( LoginMsg subMsg, Login login ) ->
            Login.update subMsg login
                |> updateWith Login LoginMsg model

        ( LoginMsg _, _ ) ->
            ( model, Cmd.none )

        ( RegisterMsg subMsg, Register register ) ->
            Register.update subMsg register
                |> updateWith Register RegisterMsg model

        ( RegisterMsg _, _ ) ->
            ( model, Cmd.none )

        ( ProfileMsg subMsg, Profile profile ) ->
            Profile.update subMsg profile
                |> updateWith Profile ProfileMsg model

        ( ProfileMsg _, _ ) ->
            ( model, Cmd.none )

        ( ClickerMsg subMsg, Clicker clickers ) ->
            Clicker.update subMsg clickers
                |> updateWith Clicker ClickerMsg model

        ( ClickerMsg _, _ ) ->
            ( model, Cmd.none )

        ( ConditionsMsg subMsg, Conditions conditions ) ->
            Conditions.update subMsg conditions
                |> updateWith Conditions ConditionsMsg model

        ( ConditionsMsg _, _ ) ->
            ( model, Cmd.none )

        ( ScoresMsg subMsg, Scores scores ) ->
            Scores.update subMsg scores
                |> updateWith Scores ScoresMsg model

        ( ScoresMsg _, _ ) ->
            ( model, Cmd.none )

        ( AdminGamesMsg subMsg, AdminGames adminGames ) ->
            AdminGames.update subMsg adminGames
                |> updateWith AdminGames AdminGamesMsg model

        ( AdminGamesMsg _, _ ) ->
            ( model, Cmd.none )

        ( AdminPlayersMsg subMsg, AdminPlayers adminPlayers ) ->
            AdminPlayers.update subMsg adminPlayers
                |> updateWith AdminPlayers AdminPlayersMsg model

        ( AdminPlayersMsg _, _ ) ->
            ( model, Cmd.none )

        ( AdminLockpickMsg subMsg, AdminLockpick adminLockpick ) ->
            AdminLockpick.update subMsg adminLockpick
                |> updateWith AdminLockpick AdminLockpickMsg model

        ( AdminLockpickMsg _, _ ) ->
            ( model, Cmd.none )

        ( GotSession session, Redirect _ ) ->
            ( Redirect session
            , Route.replaceUrl (Session.navKey session) Route.Home
            )

        ( GotSession _, _ ) ->
            ( model, Cmd.none )

        ( LoggedOut _, _ ) ->
            ( model, Cmd.none )


updateWith : (subModel -> Model) -> (subMsg -> Msg) -> Model -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg _ ( subModel, subCmd ) =
    ( toModel subModel, Cmd.map toMsg subCmd )


main : Program Value Model Msg
main =
    Api.application Viewer.decoder
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }
