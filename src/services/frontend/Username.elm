module Username exposing (Username, decoder, encode, toHtml, toString)

import Html exposing (Html)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)


type Username
    = Username String


toString : Username -> String
toString (Username s) =
    s


decoder : Decoder Username
decoder =
    Decode.map Username Decode.string


encode : Username -> Value
encode (Username s) =
    Encode.string s


toHtml : Username -> Html msg
toHtml (Username s) =
    Html.text s
