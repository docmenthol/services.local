module Elements.ProgressBar exposing (view)

import Html exposing (Html, div, text)
import Html.Attributes exposing (class, style)


view : String -> Int -> Html msg
view colorClass percent =
    let
        p =
            String.fromInt percent ++ "%"
    in
    div
        [ class "shadow w-full bg-gray-dark" ]
        [ div
            [ class colorClass
            , class "text-xs leading-none py-1 text-center text-white"
            , style "width" p
            ]
            [ text p ]
        ]
