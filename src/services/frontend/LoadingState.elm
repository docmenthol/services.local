module LoadingState exposing (LoadingState(..), map, unwrap)


type LoadingState err a
    = NotLoading
    | Loading
    | Loaded a
    | Failed err


unwrap : LoadingState err a -> Maybe a
unwrap loadingState =
    case loadingState of
        Loaded a ->
            Just a

        _ ->
            Nothing


map : LoadingState err a -> (a -> a) -> LoadingState err a
map loadingState f =
    case unwrap loadingState of
        Just ls ->
            Loaded <| f ls

        Nothing ->
            loadingState
