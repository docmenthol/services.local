module Session exposing (Session(..), changes, cred, fromViewer, navKey, profile, viewer)

import Api exposing (Cred)
import Api.Profile exposing (Profile(..))
import Browser.Navigation as Nav
import Viewer exposing (Viewer)


type Session
    = Authenticated Nav.Key Viewer Profile
    | Guest Nav.Key


viewer : Session -> Maybe Viewer
viewer session =
    case session of
        Authenticated _ viewerVal _ ->
            Just viewerVal

        Guest _ ->
            Nothing


cred : Session -> Maybe Cred
cred session =
    case session of
        Authenticated _ viewerVal _ ->
            Just (Viewer.cred viewerVal)

        Guest _ ->
            Nothing


profile : Session -> Maybe Profile
profile session =
    case session of
        Authenticated _ _ profileVal ->
            Just profileVal

        Guest _ ->
            Nothing


navKey : Session -> Nav.Key
navKey session =
    case session of
        Authenticated key _ _ ->
            key

        Guest key ->
            key


changes : (Session -> msg) -> Nav.Key -> Sub msg
changes toMsg key =
    Api.viewerChanges
        (\maybeViewer -> toMsg <| fromViewer key maybeViewer)
        Viewer.decoder


fromViewer : Nav.Key -> Maybe Viewer -> Session
fromViewer key maybeViewer =
    case maybeViewer of
        Just viewerVal ->
            Authenticated key viewerVal EmptyProfile

        Nothing ->
            Guest key
