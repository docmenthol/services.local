const purgeFromElm = content => {
  const htmlMatch = content.match(/import Html exposing \((.*?)\)/)
  const tags =
    htmlMatch
      ? htmlMatch[1]
          .replace('Attribute, ', '')
          .replace('Html, ', '')
          .replace('text, ', '')
          .replace('Attribute', '')
          .replace('Html', '')
          .replace('text', '')
          .replace('_', '')
          .split(', ')
          .filter(t => t.length > 0)
      : []

  const classMatches = content.match(/class \"(.*?)\"/g)
  const classes = classMatches
    ? classMatches.map(c =>
        c.replace('class ', '')
         .replace(/"/g, '')
         .split(' ')).flat(2)
    : []

  const selectors =
    [ ...tags, ...classes ]
      .map(s => s.replace(/"/g, ''))

  return selectors
}

module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
    require('@fullhuman/postcss-purgecss')({
      content: ['./resources/public/index.html', './src/services/frontend/**/*.elm'],
      css: ['./resources/css/*.css'],
      extractors: [{ extractor: purgeFromElm, extensions: ['elm'] }],
      whitelist: ['editing'],
      whitelistPatterns: [/table__.*?/, /autotable.*?/]
    }),
    require('cssnano')({ preset: 'advanced' })
  ]
}
