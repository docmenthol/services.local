# services.local

LAN services.

Provides a suite of LAN-hosted services like local game leaderboards, internal
climate conditions, and whatever else I feel like adding. Currently written with
Elm and Clojure, looking to move to a Rust or Haskell backend.

 UI | API | CSS
--- | --- | ---
[![UI](https://gitlab.com/docmenthol/services.local/badges/master/pipeline.svg?job=test_build_ui)](https://gitlab.com/docmenthol/services.local/-/jobs) | [![API](https://gitlab.com/docmenthol/services.local/badges/master/pipeline.svg?job=test_build_api)](https://gitlab.com/docmenthol/services.local/-/jobs) | [![CSS](https://gitlab.com/docmenthol/services.local/badges/master/pipeline.svg?job=test_build_css)](https://gitlab.com/docmenthol/services.local/-/jobs) 

## Requirements

* NodeJS
* Yarn
* Java
* Leiningen

## Setup

#### Linux

* Install `yarn` and `leiningen` with your package manager. You know how to do
  that.
* Go to Finishing Up.

#### Windows (Easy Way)

* [Install Scoop.](https://scoop.sh/)
* Install `yarn` and `leiningen` with scoop.
* Go to Finishing Up.

#### Windows (Hard Way)

* [Install Yarn.](https://yarnpkg.com/en/docs/install#windows-stable)
* [Install Leiningen.](https://leiningen.org/)
* [Add them to your PATH environment variable.](https://superuser.com/a/284351)
* Go to Finishing Up.

#### Finishing Up


* Install dependencies.

`$ yarn install && lein deps`

* Create a directory named `db` insite `resources/`.
* Bootstrap the database.

`$ lein migratus up`

## Running the Frontend

`$ yarn start`

## Running the Backend

`$ yarn start-api`
